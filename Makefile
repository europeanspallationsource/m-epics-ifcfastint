include ${EPICS_ENV_PATH}/module.Makefile

EXCLUDE_ARCHS = centos

# We need an explicit dependency on nds3epics.
USR_DEPENDENCIES = nds3epics,1.0+
# Temporary dependency
USR_DEPENDENCIES += ifcdaqdrv,niklasclaesson

USR_CXXFLAGS = -std=c++0x
OPIS = opi

# Enable debug, disabled by default.
#CROSS_OPT = NO
#USR_CPPFLAGS = -DDEBUG
