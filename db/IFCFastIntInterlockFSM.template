record(mbbo, "${PREFIX}:${CH_GRP_ID}-STAT") {
    field(DESC, "Set local state")
    field(DTYP, "asynInt32")
    field(SCAN, "Passive")
    field(OUT, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)StateMachine-setState")
    field(ZRVL, 0)
    field(ZRST, "UNKNOWN")
    field(ONVL, 1)
    field(ONST, "OFF")
    field(TWVL, 2)
    field(TWST, "SWITCHING_OFF")
    field(THVL, 3)
    field(THST, "INITIALIZING")
    field(FRVL, 4)
    field(FRST, "ON")
    field(FVVL, 5)
    field(FVST, "STOPPING")
    field(SXVL, 6)
    field(SXST, "STARTING")
    field(SVVL, 7)
    field(SVST, "RUNNING")
    field(EIVL, 8)
    field(EIST, "FAULT")
}

record(mbbi, "${PREFIX}:${CH_GRP_ID}-STAT-RB") {
    field(DESC, "Get local state")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(PINI, "YES")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0) StateMachine-getState")
    field(ZRVL, 0)
    field(ZRST, "UNKNOWN")
    field(ONVL, 1)
    field(ONST, "OFF")
    field(TWVL, 2)
    field(TWST, "SWITCHING_OFF")
    field(THVL, 3)
    field(THST, "INITIALIZING")
    field(FRVL, 4)
    field(FRST, "ON")
    field(FVVL, 5)
    field(FVST, "STOPPING")
    field(SXVL, 6)
    field(SXST, "STARTING")
    field(SVVL, 7)
    field(SVST, "RUNNING")
    field(EIVL, 8)
    field(EIST, "FAULT")
    field(EISV, "MAJOR")
}

record(mbbi, "${PREFIX}:${CH_GRP_ID}-INTS-RB") {
    field(DESC, "Get interlock state")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(PINI, "YES")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)InterlockState-RB")
    field(ZRVL, 0)
    field(ZRST, "IDLE")
    field(ONVL, 1)
    field(ONST, "ARM")
    field(TWVL, 2)
    field(TWST, "PRE")
    field(THVL, 3)
    field(THST, "RUN")
    field(FRVL, 4)
    field(FRST, "ABO")
    field(FVVL, 5)
}

record(longout, "${PREFIX}:${CH_GRP_ID}-SMNM") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "Passive")
    field(VAL,  "$(SMNM=1024)")
    field(OUT, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)NSamples")
}

record(longin, "${PREFIX}:${CH_GRP_ID}-SMNM-RB") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(PINI, "YES")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)NSamples-RB")
}

record(mbbo, "${PREFIX}:${CH_GRP_ID}-FREQ") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "Passive")
    field(OUT, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)Frequency")
    field(ZRVL, 200)
    field(ZRST, "200 kHz")
    field(ONVL, 500)
    field(ONST, "500 kHz")
    field(TWVL, 1000)
    field(TWST, "1000 kHz")
}

record(mbbi, "${PREFIX}:${CH_GRP_ID}-FREQ-RB") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(PINI, "YES")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)Frequency-RB")
    field(ZRVL, 200)
    field(ZRST, "200 kHz")
    field(ONVL, 500)
    field(ONST, "500 kHz")
    field(TWVL, 1000)
    field(TWST, "1000 kHz")
}

#record(bo, "${PREFIX}:${CH_GRP_ID}-PP-LOCK") {
#    field(DESC, "")
#    field(DTYP, "asynInt32")
#    field(SCAN, "Passive")
#    field(PINI, "YES")
#    field(OUT, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)PP-Lock")
#}

record(bi, "${PREFIX}:${CH_GRP_ID}-PP-LOCK-RB") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(PINI, "YES")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)PP-Lock-RB")
}

# Maybe implement a way to set Clock settings for both groups at the same time...
#record(mbbo, "${PREFIX}:${CH_GRP_ID}-CLKS") {
#    field(DESC, "")
#    field(DTYP, "asynInt32")
#    field(SCAN, "Passive")
#    field(OUT, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)ClockSource")
#    field(ZRVL, 0)
#    field(ZRST, "Internal")
#    field(ONVL, 1)
#    field(ONST, "External")
#}
#
#record(mbbi, "${PREFIX}:${CH_GRP_ID}-CLKS-RB") {
#    field(DESC, "")
#    field(DTYP, "asynInt32")
#    field(SCAN, "I/O Intr")
#    field(PINI, "YES")
#    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)ClockSource-RB")
#    field(ZRVL, 0)
#    field(ZRST, "Internal")
#    field(ONVL, 1)
#    field(ONST, "External")
#}

#record(ao, "${PREFIX}:${CH_GRP_ID}-CLKF") {
#    field(DESC, "")
#    field(DTYP, "asynFloat64")
#    field(SCAN, "Passive")
#    field(OUT, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)ClockFrequency")
#}
#
#record(ai, "${PREFIX}:${CH_GRP_ID}-CLKF-RB") {
#    field(DESC, "")
#    field(DTYP, "asynFloat64")
#    field(SCAN, "I/O Intr")
#    field(PINI, "YES")
#    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)ClockFrequency-RB")
#}

record(waveform, "${PREFIX}:${CH_GRP_ID}-INFO") {
    field(DESC, "")
    field(DTYP, "asynInt8ArrayIn")
    field(FTVL, "CHAR")
    field(NELM, 512)
    field(SCAN, "I/O Intr")
    field(PINI, "YES")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)InfoMessage")
}

record(waveform, "${PREFIX}:${CH_GRP_ID}-MSGS") {
    field(DESC, "")
    field(DTYP, "asynInt8ArrayOut")
    field(FTVL, "CHAR")
    field(NELM, 512)
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)Command")
}

record(bi, "${PREFIX}:${CH_GRP_ID}-OUT0") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)FSMOut0")
}

record(bi, "${PREFIX}:${CH_GRP_ID}-OUT1") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)FSMOut1")
}

record(bi, "${PREFIX}:${CH_GRP_ID}-OUT2") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)FSMOut2")
}

record(bi, "${PREFIX}:${CH_GRP_ID}-OUT3") {
    field(DESC, "")
    field(DTYP, "asynInt32")
    field(SCAN, "I/O Intr")
    field(INP, "@asyn(${ASYN_PORT=${PREFIX}}-${CH_GRP_ID}, 0)FSMOut3")
}
