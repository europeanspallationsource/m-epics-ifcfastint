#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>
#include <unistd.h>
#include <inttypes.h>

#include <iomanip>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "IFCFastInt.h"
#include "IFCFastIntAIChannelGroup.h"
#include "IFCFastIntAIChannel.h"
#include "IFCFastIntDIChannelGroup.h"
#include "IFCFastIntDIChannel.h"
#include "IFCFastIntInterlockFSM.h"

IFCFastIntInterlockFSM::IFCFastIntInterlockFSM(const std::string &name,
                                               nds::Node &parentNode,
                                               ifcdaqdrv_usr &deviceUser)
    : m_node(nds::Port(name, nds::nodeType_t::generic)),
      m_deviceUser(deviceUser), m_nFrames(1000), m_fsmFrequency(1000),
      m_ppconf_locked(1),
      m_infoPV(nds::PVDelegateIn<std::string>(
          "InfoMessage",
          std::bind(&IFCFastIntInterlockFSM::getInfoMessage, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_nSamplesPV(nds::PVDelegateIn<std::int32_t>(
          "NSamples-RB",
          std::bind(&IFCFastIntInterlockFSM::getNSamples, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_fsmFrequencyPV(nds::PVDelegateIn<std::int32_t>(
          "Frequency-RB",
          std::bind(&IFCFastIntInterlockFSM::getFrequency, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_intFSMPV(nds::PVDelegateIn<std::int32_t>(
          "InterlockState-RB",
          std::bind(&IFCFastIntInterlockFSM::getInterlockState, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pplockPV(nds::PVDelegateIn<std::int32_t>(
          "PP-Lock-RB",
          std::bind(&IFCFastIntInterlockFSM::getPPLock, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_fsmOut0PV(nds::PVDelegateIn<std::int32_t>(
          "FSMOut0", std::bind(&IFCFastIntInterlockFSM::getFSMOut, this,
                               std::placeholders::_1, std::placeholders::_2))),
      m_fsmOut1PV(nds::PVDelegateIn<std::int32_t>(
          "FSMOut1", std::bind(&IFCFastIntInterlockFSM::getFSMOut, this,
                               std::placeholders::_1, std::placeholders::_2))),
      m_fsmOut2PV(nds::PVDelegateIn<std::int32_t>(
          "FSMOut2", std::bind(&IFCFastIntInterlockFSM::getFSMOut, this,
                               std::placeholders::_1, std::placeholders::_2))),
      m_fsmOut3PV(nds::PVDelegateIn<std::int32_t>(
          "FSMOut3", std::bind(&IFCFastIntInterlockFSM::getFSMOut, this,
                               std::placeholders::_1, std::placeholders::_2))) {
  // ifcdaqdrv_status status;

  struct timespec now = {0, 0};
  clock_gettime(CLOCK_REALTIME, &now);

  parentNode.addChild(m_node);

#define HISTORY_FRAME_SIZE 64
#define HISTORY_FRAME_COUNT_MAX (1 * 1024)

  m_rawData = (int8_t *)calloc(HISTORY_FRAME_SIZE * HISTORY_FRAME_COUNT_MAX,
                               sizeof(int8_t));
  if (!m_rawData) {
    throw nds::NdsError("Out of memory");
  }

  // PV for commands like abort
  m_node.addChild(nds::PVDelegateOut<std::string>(
      "Command", std::bind(&IFCFastIntInterlockFSM::sendCommand, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntInterlockFSM::recvCommand, this,
                std::placeholders::_1, std::placeholders::_2)));

  // PVs for Number of Samples.
  m_node.addChild(nds::PVDelegateOut<std::int32_t>(
      "NSamples", std::bind(&IFCFastIntInterlockFSM::setNSamples, this,
                            std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntInterlockFSM::getNSamples, this,
                std::placeholders::_1, std::placeholders::_2)));

  m_nSamplesPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_nSamplesPV);

  // PVs for FSM Frequency
  m_node.addChild(nds::PVDelegateOut<std::int32_t>(
      "Frequency", std::bind(&IFCFastIntInterlockFSM::setFrequency, this,
                             std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntInterlockFSM::getFrequency, this,
                std::placeholders::_1, std::placeholders::_2)));

  m_fsmFrequencyPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmFrequencyPV);

  // PVs for FSM state
  nds::enumerationStrings_t interlockStateStrings;
  interlockStateStrings.push_back("IDLE");
  interlockStateStrings.push_back("ARM");
  interlockStateStrings.push_back("PRE");
  interlockStateStrings.push_back("RUN");
  interlockStateStrings.push_back("ABO");

  // m_intFSMPV.setScanType(nds::scanType_t::interrupt);
  // m_intFSMPV.setScanType(nds::scanType_t::periodic);
  m_intFSMPV.setEnumeration(interlockStateStrings);
  m_node.addChild(m_intFSMPV);

  // PV for debug/info messages.
  m_infoPV.setScanType(nds::scanType_t::interrupt);
  m_infoPV.setMaxElements(512);
  m_node.addChild(m_infoPV);

  // PVs for state machine.
  m_stateMachine = m_node.addChild(nds::StateMachine(
      true, std::bind(&IFCFastIntInterlockFSM::onSwitchOn, this),
      std::bind(&IFCFastIntInterlockFSM::onSwitchOff, this),
      std::bind(&IFCFastIntInterlockFSM::onStart, this),
      std::bind(&IFCFastIntInterlockFSM::onStop, this),
      std::bind(&IFCFastIntInterlockFSM::recover, this),
      std::bind(&IFCFastIntInterlockFSM::allowChange, this,
                std::placeholders::_1, std::placeholders::_2,
                std::placeholders::_3)));

  m_fsmOut0PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmOut0PV);
  m_fsmOut1PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmOut1PV);
  m_fsmOut2PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmOut2PV);
  m_fsmOut3PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmOut3PV);

  // PVs for FSM Frequency
  // m_node.addChild(nds::PVDelegateOut<std::int32_t>("PP-Lock",
  //                                               std::bind(&IFCFastIntInterlockFSM::setPPLock,
  //                                                         this,
  //                                                         std::placeholders::_1,
  //                                                         std::placeholders::_2),
  //                                               std::bind(&IFCFastIntInterlockFSM::getPPLock,
  //                                                         this,
  //                                                         std::placeholders::_1,
  //                                                         std::placeholders::_2)));

  m_pplockPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pplockPV);

  // Create DI/AI groups
  m_AIGroups.push_back(std::make_shared<IFCFastIntAIChannelGroup>(
      "AI0", m_node, m_deviceUser, 0));
  m_AIGroups.push_back(std::make_shared<IFCFastIntAIChannelGroup>(
      "AI1", m_node, m_deviceUser, 8));
  m_DIGroup =
      std::make_shared<IFCFastIntDIChannelGroup>("DI", m_node, m_deviceUser);

  std::cout << "unlock" << std::endl;
  ifcfastint_conf_unlock(&m_deviceUser);
  m_ppconf_locked = 0;

  // Initialize some values from hardware
  // uint32_t u32_reg_val;
  // status = ifcfastint_get_fsm_frequency(&deviceUser, &u32_reg_val);
  // m_fsmFrequency = u32_reg_val;
  // m_fsmFrequencyPV.read(&now, &m_fsmFrequency);
  // m_fsmFrequencyPV.push(now, m_fsmFrequency);
  // if(status)
  //    printf("fuu\n");
}

/*
 * Round upwards to nearest power-of-2.
 */

static inline int ceil_pow2(unsigned number) {
  unsigned n = 1;
  unsigned i = number - 1;
  while (i) {
    n <<= 1;
    i >>= 1;
  }
  return n;
}

/*
 * Round upwards to nearest mibibytes
 */

static inline int ceil_mibi(unsigned number) {
  return (1 + (number - 1) / (1024 * 1024)) * 1024 * 1024;
}

void IFCFastIntInterlockFSM::sendCommand(const timespec &timespec,
                                         const std::string &value) {
  if (value.compare(0, 5, "ABORT") == 0) {
    ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_abort);
  } else if (value.compare(0, 3, "PRE") == 0) {
    ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_pre);
  } else if (value.compare(0, 3, "RUN") == 0) {
    ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_run);
  } else if (value.compare(0, 4, "IDLE") == 0) {
    ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_idle);
  } else {
    ndsWarningStream(m_node) << "Unidentified message: " << value << std::endl;
  }
  struct timespec now;
  int32_t tmp;
  m_intFSMPV.read(&now, &tmp);
  m_intFSMPV.push(now, tmp);
}
void IFCFastIntInterlockFSM::recvCommand(timespec *timespec,
                                         std::string *value) {}
void IFCFastIntInterlockFSM::getInfoMessage(timespec *timespec,
                                            std::string *value) {
  std::ostringstream tmp;
  char manufacturer[100];
  ifcdaqdrv_get_manufacturer(&m_deviceUser, manufacturer, 100);
  char product_name[100];
  ifcdaqdrv_get_product_name(&m_deviceUser, product_name, 100);
  tmp << manufacturer << " " << product_name;
  *value = tmp.str();
}

void IFCFastIntInterlockFSM::getInterlockState(timespec *timespec,
                                               int32_t *value) {
  ifcfastint_fsm_state state;
  clock_gettime(CLOCK_REALTIME, timespec);
  ifcfastint_get_fsm_state(&m_deviceUser, &state);
  switch (state) {
  case ifcfastint_fsm_state_idle:
    *value = 0;
    return;
  case ifcfastint_fsm_state_arm:
    *value = 1;
    return;
  case ifcfastint_fsm_state_pre:
    *value = 2;
    return;
  case ifcfastint_fsm_state_run:
    *value = 3;
    return;
  case ifcfastint_fsm_state_abort:
    *value = 4;
    return;
  }
}

void IFCFastIntInterlockFSM::getNSamples(timespec *timespec, int32_t *value) {
  clock_gettime(CLOCK_REALTIME, timespec);
  *value = m_nFrames;
}

void IFCFastIntInterlockFSM::setNSamples(const timespec &timespec,
                                         const int32_t &value) {
  struct timespec tmp;
  int32_t rb_value;

  // if(value > HISTORY_FRAME_COUNT_MAX) {
  //    IFCFastIntNDS_MSGWRN("Buffer not large enough for samples. Dynamic
  //    allocation not implemented yet.");
  //    return;
  //}
  m_nFrames = value;
  m_nSamplesPV.read(&tmp, &rb_value);
  m_nSamplesPV.push(timespec, rb_value);
}

void IFCFastIntInterlockFSM::updatePPLock() {
  struct timespec timespec;
  int32_t rb_value;

  m_pplockPV.read(&timespec, &rb_value);
  m_pplockPV.push(timespec, rb_value);
}

void IFCFastIntInterlockFSM::getPPLock(timespec *timespec, int32_t *value) {
  clock_gettime(CLOCK_REALTIME, timespec);
  *value = m_ppconf_locked;
}

// void IFCFastIntInterlockFSM::setPPLock(const timespec& timespec, const
// int32_t& value)
//{
//    struct timespec now;
//    int32_t rb_value;
//    if(value) {
//        m_ppconf_locked = 1;
//        ifcfastint_conf_lock(&m_deviceUser);
//    } else {
//        m_ppconf_locked = 0;
//        ifcfastint_conf_unlock(&m_deviceUser);
//    }
//
//    //m_pplockPV.read(&now, &rb_value);
//    //m_pplockPV.push(now, rb_value);
//}

void IFCFastIntInterlockFSM::getFrequency(timespec *timespec, int32_t *value) {
  uint32_t u32_reg_val;
  ifcfastint_get_fsm_frequency(&m_deviceUser, &u32_reg_val);
  *value = u32_reg_val;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntInterlockFSM::setFrequency(const timespec &timespec,
                                          const int32_t &value) {
  struct timespec now;
  int32_t rb_value;

  ifcfastint_set_fsm_frequency(&m_deviceUser, value);
  m_fsmFrequencyPV.read(&now, &rb_value);
  m_fsmFrequencyPV.push(now, rb_value);
}

void IFCFastIntInterlockFSM::onSwitchOn() {
  // Unlock PP Options
  ifcfastint_conf_unlock(&m_deviceUser);
  m_ppconf_locked = 0;
  updatePPLock();

  // Enable all channels
  for (auto const &group : m_AIGroups) {
    group->setState(nds::state_t::on);
  }
}

void IFCFastIntInterlockFSM::onSwitchOff() {
  // Disable all channels
  for (auto const &group : m_AIGroups) {
    group->setState(nds::state_t::off);
  }
}

void IFCFastIntInterlockFSM::onStart() {
  /* Start all Channels */
  for (auto const &group : m_AIGroups) {
    group->setState(nds::state_t::running);
  }

  ifcfastint_fsm_reset(&m_deviceUser);

  ifcfastint_conf_lock(&m_deviceUser);
  m_ppconf_locked = 1;

  updatePPLock();

  // Start data acquisition
  m_stop = false;
  m_acquisitionThread = m_node.runInThread(
      "AcquisitionLoop",
      std::bind(&IFCFastIntInterlockFSM::acquisitionLoop, this, m_nFrames));
}

void IFCFastIntInterlockFSM::onStop() {
  //m_stop = true;
  // ifcdaqdrv_disarm_device(&m_deviceUser);
  ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_abort);

  m_acquisitionThread.join();
  // Stop channels
  for (auto const &group : m_AIGroups) {
    group->setState(nds::state_t::on);
  }

  ifcfastint_conf_unlock(&m_deviceUser);
  m_ppconf_locked = 0;

  updatePPLock();

  // commitParameters();
}

void IFCFastIntInterlockFSM::recover() {
  throw nds::StateMachineRollBack("Cannot recover");
}

bool IFCFastIntInterlockFSM::allowChange(const nds::state_t currentLocal,
                                         const nds::state_t currentGlobal,
                                         const nds::state_t nextLocal) {
  return true;
}

void IFCFastIntInterlockFSM::getFSMOut(timespec *timespec, int32_t *value) {}

/* Convert from [0=-1, 32k=0, 64k=1] to [-32k=-1, 0=0, 32k-1=1] */
inline int32_t raw_to_signed(int16_t *raw) { return (int16_t)(*raw - 32768); }

/* Convert from [-32k=-1, 0=0, 32k-1=1] to [0=-1, 32k=0, 64k=1] */
inline int16_t signed_to_raw(int32_t *signd) {
  return (int16_t)(*signd + 32768);
}

void IFCFastIntInterlockFSM::acquisitionLoop(int32_t nframes) {
  ndsInfoStream(m_node) << "Acquisition thread started" << std::endl;
  ifcdaqdrv_status status;
  ifcfastint_fsm_state state;
  struct timespec now;

  // Reserve all memory
  m_rawData = (int8_t *)realloc(m_rawData, HISTORY_FRAME_SIZE * nframes);

  status = ifcfastint_get_fsm_state(&m_deviceUser, &state);
  if (status) {
    ndsErrorStream(m_node) << "Failed to read state" << std::endl;
  }

  while (!m_stop && state != ifcfastint_fsm_state_abort) {
    size_t nFrames;

    for (auto channel : m_AIGroups[0]->getChannels()) {
      channel->reserve(nframes);
    }
    for (auto channel : m_AIGroups[1]->getChannels()) {
      channel->reserve(nframes);
    }
    for (auto channel : m_DIGroup->getChannels()) {
      channel->reserve(nframes);
    }

    clock_gettime(CLOCK_REALTIME, &now);

    /* Get a single history frame from the device */
    status = ifcfastint_read_history(&m_deviceUser, nframes, m_rawData, &nFrames);
    if (status) {
      ndsErrorStream(m_node) << "Failed to read history" << std::endl;
      break;
    }

    ndsInfoStream(m_node) << "Read history frames" << std::endl;

    /* For debugging purposes this can handle multiple frames */
    for (int8_t *raw(m_rawData);
         raw < m_rawData + (nFrames * HISTORY_FRAME_SIZE);
         raw += HISTORY_FRAME_SIZE) {
      int16_t *i16_raw = (int16_t *)raw;
      uint16_t *u16_raw = (uint16_t *)raw;
      uint32_t digital_pp_qout;
      uint32_t analog_pp_qout;
      int32_t k = (raw - m_rawData) / HISTORY_FRAME_SIZE;

      std::ostringstream debug_ss;

      uint32_t frame_num =
          ((uint32_t) * (u16_raw + 3) << 16) + (uint32_t) * (u16_raw + 2);
      debug_ss << "FRAME " << std::hex << frame_num <<
        " (" << std::dec << k << ")" <<
        std::endl;
      //printf("%d\n", k);
      for (int j = 0; j < 16; j++) {
        debug_ss << std::hex << std::setw(8) << std::setfill('0') << *(((int32_t *)raw) + j) << " ";
        if (j == 7) debug_ss << std::endl;
      }
      ndsDebugStream(m_node) << debug_ss.str() << std::endl;

      // The PP QOUT is the results of the pre-processing blocks.
      // They are stored such that the higher 16 bits are the lower
      // 16 channels and the lower 16 bits are the higher channels.
      analog_pp_qout = *((uint32_t *)raw + 3);
      digital_pp_qout = *((uint32_t *)raw + 2);
      size_t i = 0;
      for (auto channel : m_AIGroups[0]->getChannels()) {
        channel->historyAddValue(raw_to_signed(i16_raw + (10 + i)));
        // Update current value and qout every 100 values
        if(k%100 == 0) {
          size_t qout_idx = (16 + i) % 32;
          channel->addValue(raw_to_signed(i16_raw + (10 + i)));
          channel->setPPOutput((analog_pp_qout & (1 << qout_idx)) != 0);
        }
        ++i;
      }
      for (auto channel : m_AIGroups[1]->getChannels()) {
        channel->historyAddValue(raw_to_signed(i16_raw + (10 + i)));
        // Update current value and qout every 100 values
        if(k%100 == 0) {
          size_t qout_idx = (16 + i) % 32;
          channel->addValue(raw_to_signed(i16_raw + (10 + i)));
          channel->setPPOutput((analog_pp_qout & (1 << qout_idx)) != 0);
        }
        ++i;
      }
      i = 0;
      for (auto channel : m_DIGroup->getChannels()) {
        size_t dig_idx = (16 + i) % 32;
        channel->historyAddValue((*((int32_t *)raw + 4) >> dig_idx) & 1);
        // Update current value and qout every 100 values
        if(k%100 == 0) {
          channel->addValue((*((int32_t *)raw + 4) >> dig_idx) & 1);
          channel->setPPOutput((digital_pp_qout & (1 << dig_idx)) != 0);
        }
        ++i;
      }
    }

    /* Push to all data to PVs and clear all vectors */
    for (auto channel : m_AIGroups[0]->getChannels()) {
      channel->historyReset();
    }
    for (auto channel : m_AIGroups[1]->getChannels()) {
      channel->historyReset();
    }
    for (auto channel : m_DIGroup->getChannels()) {
      channel->historyReset();
    }

    // Get the interlock state
    status = ifcfastint_get_fsm_state(&m_deviceUser, &state);
    if (status) {
      ndsErrorStream(m_node) << "Failed to read state" << std::endl;
      break;
    }
    m_intFSMPV.push(now, (int32_t)state);

    // Get the outputs
    status = ifcfastint_get_fsm_do(&m_deviceUser, &m_fsmOutChannelMask);
    if (status) {
      ndsErrorStream(m_node) << "Failed to get FSM OUT" << std::endl;
      break;
    }
    m_fsmOut0PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 0)) != 0));
    m_fsmOut1PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 1)) != 0));
    m_fsmOut2PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 2)) != 0));
    m_fsmOut3PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 3)) != 0));

    // Sleep 200 ms. ~5Hz is good enough update frequency for the OPI.
    usleep(200 * 1000);
  }

  if(!m_stop) {
    try {
      m_stateMachine.setState(nds::state_t::on);
    } catch (nds::StateMachineNoSuchTransition error) {
      std::cout << "Tried to go to ON but failed" << std::endl;
      // We are probably already in "stopping", no need to panic...
    }
  }

  ifcfastint_wait_abort_done(&m_deviceUser);

  if (state == ifcfastint_fsm_state_abort) {
    size_t nFrames;

    for (auto channel : m_AIGroups[0]->getChannels()) {
      channel->reserve(nframes);
    }
    for (auto channel : m_AIGroups[1]->getChannels()) {
      channel->reserve(nframes);
    }
    for (auto channel : m_DIGroup->getChannels()) {
      channel->reserve(nframes);
    }

    status =
      ifcfastint_read_history(&m_deviceUser, nframes, m_rawData, &nFrames);
    if (status) {
      ndsErrorStream(m_node) << "Failed to read history" << std::endl;
    }
    ndsDebugStream(m_node) << "Got " << nFrames << " history frames on abort"
      << std::endl;

    for (int8_t *raw(m_rawData);
        raw < m_rawData + (nFrames * HISTORY_FRAME_SIZE);
        raw += HISTORY_FRAME_SIZE) {
      int16_t *i16_raw = (int16_t *)raw;
      // printf("FRAME %08x (%d)\n", *((int32_t*)raw + 1),
      // (raw-m_rawData)/HISTORY_FRAME_SIZE);
      // for(int j=0; j<16; j++) {
      //    printf("%08x ", *(((int32_t *)raw) + j));
      //    if(j==7) printf("\n");
      //}
      // printf("\n");
      size_t i = 0;
      for (auto channel : m_AIGroups[0]->getChannels()) {
        channel->historyAddValue(raw_to_signed(i16_raw + (10 + i)));
        ++i;
      }
      for (auto channel : m_AIGroups[1]->getChannels()) {
        channel->historyAddValue(raw_to_signed(i16_raw + (10 + i)));
        ++i;
      }
      i = 0;
      for (auto channel : m_DIGroup->getChannels()) {
        size_t dig_idx = (16 + i) % 32;
        channel->historyAddValue((*((int32_t *)raw + 4) >> dig_idx) & 1);
        ++i;
      }
    }

    /* Push to all data to PVs and clear all vectors */
    for (auto channel : m_AIGroups[0]->getChannels()) {
      channel->historyReset();
    }
    for (auto channel : m_AIGroups[1]->getChannels()) {
      channel->historyReset();
    }
    for (auto channel : m_DIGroup->getChannels()) {
      channel->historyReset();
    }

    /* Read the outputs */
    status = ifcfastint_get_fsm_do(&m_deviceUser, &m_fsmOutChannelMask);
    if (status) {
      ndsErrorStream(m_node) << "Failed to get FSM OUT" << std::endl;
    }
    clock_gettime(CLOCK_REALTIME, &now);
    m_fsmOut0PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 0)) != 0));
    m_fsmOut1PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 1)) != 0));
    m_fsmOut2PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 2)) != 0));
    m_fsmOut3PV.push(now, (int32_t)((m_fsmOutChannelMask & (1 << 3)) != 0));
  }

  ndsInfoStream(m_node) << "Acquisition stopped." << std::endl;
}
