
#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "IFCFastInt.h"
#include "IFCFastIntDIChannel.h"

IFCFastIntDIChannel::IFCFastIntDIChannel(const std::string &name,
                                         nds::Node &parentNode,
                                         int32_t channelNum,
                                         ifcdaqdrv_usr &deviceUser)
    : m_channelNum(channelNum), m_deviceUser(deviceUser),
      m_dataPV(nds::PVDelegateIn<std::int32_t>(
          "Data", std::bind(&IFCFastIntDIChannel::getData, this,
                            std::placeholders::_1, std::placeholders::_2))),
      m_historyPV(nds::PVDelegateIn<std::vector<std::int32_t>>(
          "History", std::bind(&IFCFastIntDIChannel::getHistory, this,
                               std::placeholders::_1, std::placeholders::_2))),
      m_pp_outPV(nds::PVDelegateIn<std::int32_t>(
          "PPQOUT", std::bind(&IFCFastIntDIChannel::getPPQOut, this,
                              std::placeholders::_1, std::placeholders::_2))),
      m_pp_emulatedPV(nds::PVDelegateIn<std::int32_t>(
          "PP-EMUL-RB",
          std::bind(&IFCFastIntDIChannel::getPPEmulated, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_modePV(nds::PVDelegateIn<std::int32_t>(
          "PP-MODE-RB",
          std::bind(&IFCFastIntDIChannel::getPPMode, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_val1PV(nds::PVDelegateIn<std::int32_t>(
          "PP-VAL1-RB",
          std::bind(&IFCFastIntDIChannel::getPPVal1, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_val2PV(nds::PVDelegateIn<std::int32_t>(
          "PP-VAL2-RB",
          std::bind(&IFCFastIntDIChannel::getPPVal2, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_cvalPV(nds::PVDelegateIn<std::int32_t>(
          "PP-CVAL-RB",
          std::bind(&IFCFastIntDIChannel::getPPCVal, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_fsmTransIdlePrePV(nds::PVDelegateIn<std::int32_t>(
          "TRANS-IDLE2PRE-RB",
          std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionIdlePre,
                    this, std::placeholders::_1, std::placeholders::_2))),
      m_fsmTransPreRunPV(nds::PVDelegateIn<std::int32_t>(
          "TRANS-PRE2RUN-RB",
          std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionPreRun,
                    this, std::placeholders::_1, std::placeholders::_2))) {
  m_node = parentNode.addChild(nds::Node(name));

  /* Add data PV */
  m_dataPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_dataPV);

  /* Add history PV */
  m_historyPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_historyPV);

  /* Add QOUT PV */
  m_pp_outPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_outPV);

  /* Add PV for Interlock Transisition qualifier*/
  nds::PVDelegateOut<std::int32_t> node = nds::PVDelegateOut<std::int32_t>(
      "PP-EMUL", std::bind(&IFCFastIntDIChannel::setPPEmulated, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntDIChannel::getPPEmulated, this,
                std::placeholders::_1, std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_pp_emulatedPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_emulatedPV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "PP-MODE", std::bind(&IFCFastIntDIChannel::setPPMode, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntDIChannel::getPPMode, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_pp_modePV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_modePV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "PP-VAL1", std::bind(&IFCFastIntDIChannel::setPPVal1, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntDIChannel::getPPVal1, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_pp_val1PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_val1PV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "PP-VAL2", std::bind(&IFCFastIntDIChannel::setPPVal2, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntDIChannel::getPPVal2, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_pp_val2PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_val2PV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "PP-CVAL", std::bind(&IFCFastIntDIChannel::setPPCVal, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntDIChannel::getPPCVal, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_pp_cvalPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_cvalPV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "TRANS-IDLE2PRE",
      std::bind(&IFCFastIntDIChannel::setInterlockStateTransitionIdlePre, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionIdlePre, this,
                std::placeholders::_1, std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_fsmTransIdlePrePV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmTransIdlePrePV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "TRANS-PRE2RUN",
      std::bind(&IFCFastIntDIChannel::setInterlockStateTransitionPreRun, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionPreRun, this,
                std::placeholders::_1, std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_fsmTransPreRunPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmTransPreRunPV);

  // Clear the PP configuration settings
  // TODO(nc): should this be controlled from PVs?
  // struct ifcfastint_digital_option option;
  // memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  // ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, 0xff, &option);
}

void IFCFastIntDIChannel::reserve(size_t nframes) {
  m_history.resize(nframes);
  m_history_ptr = m_history.begin();
}

void IFCFastIntDIChannel::historyReset() {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  m_historyPV.push(now, m_history);
  m_history.clear();
}

void IFCFastIntDIChannel::historyAddValue(int32_t data) {
  *m_history_ptr = data;
  m_history_ptr++;
}

void IFCFastIntDIChannel::addValue(int32_t data) {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);

  m_dataPV.push(now, data);
}

void IFCFastIntDIChannel::setPPOutput(bool qout) {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);

  m_pp_outPV.push(now, (int32_t)qout);
}

void IFCFastIntDIChannel::getPPQOut(timespec *timespec, int32_t *value) {}

void IFCFastIntDIChannel::getData(timespec *timespec, int32_t *value) {}
void IFCFastIntDIChannel::getHistory(timespec *timespec,
                                     std::vector<std::int32_t> *value) {}

void IFCFastIntDIChannel::setState(nds::state_t newState) {
  m_stateMachine.setState(newState);
}

void IFCFastIntDIChannel::setPPEmulated(const timespec &timespec,
                                        const int32_t &value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  option.emulation_en = value;
  ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum,
                                 IFCFASTINT_DIGITAL_EMULATION_EN_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_pp_emulatedPV.read(&now, &rb_val);
  m_pp_emulatedPV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPEmulated(timespec *timespec, int32_t *value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);

  clock_gettime(CLOCK_REALTIME, timespec);
  *value = option.emulation_en;
}

void IFCFastIntDIChannel::setPPMode(const timespec &timespec,
                                    const int32_t &value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  switch (value) {
  case 0:
    option.mode = ifcfastint_dmode_zero;
    break;
  case 1:
    option.mode = ifcfastint_dmode_one;
    break;
  case 2:
    option.mode = ifcfastint_dmode_pass_through;
    break;
  case 3:
    option.mode = ifcfastint_dmode_invert;
    break;
  }
  ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum,
                                 IFCFASTINT_DIGITAL_MODE_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_pp_modePV.read(&now, &rb_val);
  m_pp_modePV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPMode(timespec *timespec, int32_t *value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  clock_gettime(CLOCK_REALTIME, timespec);
  ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
  switch (option.mode) {
  case ifcfastint_dmode_zero:
    *value = 0;
    break;
  case ifcfastint_dmode_one:
    *value = 1;
    break;
  case ifcfastint_dmode_pass_through:
    *value = 2;
    break;
  case ifcfastint_dmode_invert:
    *value = 3;
    break;
  }
}

void IFCFastIntDIChannel::setPPVal1(const timespec &timespec,
                                    const int32_t &value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  option.val1 = value;
  ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum,
                                 IFCFASTINT_DIGITAL_VAL1_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_pp_val1PV.read(&now, &rb_val);
  m_pp_val1PV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPVal1(timespec *timespec, int32_t *value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
  *value = option.val1;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntDIChannel::setPPVal2(const timespec &timespec,
                                    const int32_t &value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  option.val2 = value;
  ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum,
                                 IFCFASTINT_DIGITAL_VAL2_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_pp_val2PV.read(&now, &rb_val);
  m_pp_val2PV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPVal2(timespec *timespec, int32_t *value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
  *value = option.val2;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntDIChannel::setPPCVal(const timespec &timespec,
                                    const int32_t &value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  // option.cval = (int16_t)((value - m_linConvOffset) / m_linConvFactor);
  // option.cval = signed_to_raw(option.cval);
  option.cval = value;
  ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum,
                                 IFCFASTINT_DIGITAL_CVAL_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_pp_cvalPV.read(&now, &rb_val);
  m_pp_cvalPV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPCVal(timespec *timespec, int32_t *value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  // int16_t cval;
  ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
  *value = option.cval;
  // cval = raw_to_signed(option.cval);
  //*value = cval * m_linConvFactor + m_linConvOffset;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntDIChannel::setInterlockStateTransitionIdlePre(
    const timespec &timespec, const int32_t &value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  option.idle2pre = value;
  ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum,
                                 IFCFASTINT_DIGITAL_IDLE2PRE_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_fsmTransIdlePrePV.read(&now, &rb_val);
  m_fsmTransIdlePrePV.push(now, rb_val);
}

void IFCFastIntDIChannel::getInterlockStateTransitionIdlePre(timespec *timespec,
                                                             int32_t *value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
  *value = option.idle2pre;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntDIChannel::setInterlockStateTransitionPreRun(
    const timespec &timespec, const int32_t &value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  option.pre2run = value;
  ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum,
                                 IFCFASTINT_DIGITAL_PRE2RUN_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_fsmTransPreRunPV.read(&now, &rb_val);
  m_fsmTransPreRunPV.push(now, rb_val);
}

void IFCFastIntDIChannel::getInterlockStateTransitionPreRun(timespec *timespec,
                                                            int32_t *value) {
  struct ifcfastint_digital_option option;
  memset(&option, 0, sizeof(struct ifcfastint_digital_option));
  ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
  *value = option.pre2run;
  clock_gettime(CLOCK_REALTIME, timespec);
}
