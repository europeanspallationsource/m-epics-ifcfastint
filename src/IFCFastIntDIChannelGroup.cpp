#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "IFCFastInt.h"
#include "IFCFastIntDIChannelGroup.h"
#include "IFCFastIntDIChannel.h"

IFCFastIntDIChannelGroup::IFCFastIntDIChannelGroup(const std::string &name,
                                                   nds::Node &parentNode,
                                                   ifcdaqdrv_usr &deviceUser)
    : m_node(nds::Port(name, nds::nodeType_t::generic)),
      m_deviceUser(deviceUser) {
  parentNode.addChild(m_node);

  // TODO: Read from ifcdaq?
  m_numChannels = 24;

  for (size_t numChannel(0); numChannel != m_numChannels; ++numChannel) {
    std::ostringstream channelName;
    channelName << "CH" << numChannel;
    m_DIChannels.push_back(std::make_shared<IFCFastIntDIChannel>(
        channelName.str(), m_node, numChannel, m_deviceUser));
  }
}

std::vector<std::shared_ptr<IFCFastIntDIChannel>>
IFCFastIntDIChannelGroup::getChannels() {
  return m_DIChannels;
}
