
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "IFCFastInt.h"
#include "IFCFastIntDevice.h"
#include "IFCFastIntInterlockFSM.h"

IFCFastIntDevice::IFCFastIntDevice(nds::Factory &factory,
                                   const std::string &deviceName,
                                   const nds::namedParameters_t &parameters)
    : m_node(deviceName) {
  ifcdaqdrv_status status;

  /* Since this device support uses both fmc concurrently we simply always pass
   * 1 as fmc. */

  m_deviceUser = {static_cast<uint32_t>(std::stoul(parameters.at("card"))), 1,
                  0};

  status = ifcdaqdrv_open_device(&m_deviceUser);
  if (status != status_success) {
    throw nds::NdsError("Failed to open device");
  }
  status = ifcdaqdrv_init_adc(&m_deviceUser);
  if (status != status_success) {
    throw nds::NdsError("Failed to initialize ADC");
  }
  status = ifcfastint_init_fsm(&m_deviceUser);
  if (status != status_success) {
    throw nds::NdsError("Failed to initialize FSM");
  }

  m_IFSM =
      std::make_shared<IFCFastIntInterlockFSM>("FSM", m_node, m_deviceUser);

  // Initialize device
  m_node.initialize(this, factory);
}

IFCFastIntDevice::~IFCFastIntDevice() { ifcdaqdrv_close_device(&m_deviceUser); }

NDS_DEFINE_DRIVER(ifcfastint, IFCFastIntDevice);
