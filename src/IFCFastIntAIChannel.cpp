
#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "IFCFastInt.h"
#include "IFCFastIntAIChannel.h"

IFCFastIntAIChannel::IFCFastIntAIChannel(const std::string &name,
                                         nds::Node &parentNode,
                                         int32_t channelNum,
                                         ifcdaqdrv_usr &deviceUser,
                                         double linConvFactor)
    : m_channelNum(channelNum), m_deviceUser(deviceUser), m_linConvFactor(1),
      m_linConvOffset(0), m_decimationFactor(1), m_gain(1), m_pattern(0),
      m_linConvFactorNext(0), m_linConvOffsetNext(0), m_decimationFactorNext(1),
      m_decimationOffsetNext(0), m_gainNext(1), m_patternNext(0),
      m_linConvFactorChanged(true), m_linConvOffsetChanged(true),
      m_decimationFactorChanged(true), m_decimationOffsetChanged(true),
      m_gainChanged(true), m_patternChanged(true),
      m_dataPV(nds::PVDelegateIn<double>(
          "Data", std::bind(&IFCFastIntAIChannel::getData, this,
                            std::placeholders::_1, std::placeholders::_2))),
      m_historyPV(nds::PVDelegateIn<std::vector<double>>(
          "History", std::bind(&IFCFastIntAIChannel::getHistory, this,
                               std::placeholders::_1, std::placeholders::_2))),
      m_linConvFactorPV(nds::PVDelegateIn<double>(
          "LinearConversionFactor-RB",
          std::bind(&IFCFastIntAIChannel::getLinearConversionFactor, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_linConvOffsetPV(nds::PVDelegateIn<double>(
          "LinearConversionOffset-RB",
          std::bind(&IFCFastIntAIChannel::getLinearConversionOffset, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_decimationFactorPV(nds::PVDelegateIn<std::int32_t>(
          "DecimationFactor-RB",
          std::bind(&IFCFastIntAIChannel::getDecimationFactor, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_gainPV(nds::PVDelegateIn<double>(
          "Gain-RB", std::bind(&IFCFastIntAIChannel::getGain, this,
                               std::placeholders::_1, std::placeholders::_2))),
      m_patternPV(nds::PVDelegateIn<std::int32_t>(
          "Pattern-RB",
          std::bind(&IFCFastIntAIChannel::getPattern, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_outPV(nds::PVDelegateIn<std::int32_t>(
          "PPQOUT", std::bind(&IFCFastIntAIChannel::getPPQOut, this,
                              std::placeholders::_1, std::placeholders::_2))),
      //    m_pp_activePV(nds::PVDelegateIn<std::int32_t>("PP-ACT-RB",
      //                                                    std::bind(&IFCFastIntAIChannel::getPPActive,
      //                                                              this,
      //                                                              std::placeholders::_1,
      //                                                              std::placeholders::_2))),
      m_pp_emulatedPV(nds::PVDelegateIn<std::int32_t>(
          "PP-EMUL-RB",
          std::bind(&IFCFastIntAIChannel::getPPEmulated, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_modePV(nds::PVDelegateIn<std::int32_t>(
          "PP-MODE-RB",
          std::bind(&IFCFastIntAIChannel::getPPMode, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_val1PV(nds::PVDelegateIn<double>(
          "PP-VAL1-RB",
          std::bind(&IFCFastIntAIChannel::getPPVal1, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_val2PV(nds::PVDelegateIn<double>(
          "PP-VAL2-RB",
          std::bind(&IFCFastIntAIChannel::getPPVal2, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_val3PV(nds::PVDelegateIn<double>(
          "PP-VAL3-RB",
          std::bind(&IFCFastIntAIChannel::getPPVal3, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_val4PV(nds::PVDelegateIn<double>(
          "PP-VAL4-RB",
          std::bind(&IFCFastIntAIChannel::getPPVal4, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_pp_cvalPV(nds::PVDelegateIn<double>(
          "PP-CVAL-RB",
          std::bind(&IFCFastIntAIChannel::getPPCVal, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_fsmTransIdlePrePV(nds::PVDelegateIn<std::int32_t>(
          "TRANS-IDLE2PRE-RB",
          std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionIdlePre,
                    this, std::placeholders::_1, std::placeholders::_2))),
      m_fsmTransPreRunPV(nds::PVDelegateIn<std::int32_t>(
          "TRANS-PRE2RUN-RB",
          std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionPreRun,
                    this, std::placeholders::_1, std::placeholders::_2))) {
  // ifcdaqdrv_status status;
  m_node = parentNode.addChild(nds::Node(name));

  m_stateMachine = nds::StateMachine(
      true, std::bind(&IFCFastIntAIChannel::switchOn, this),
      std::bind(&IFCFastIntAIChannel::switchOff, this),
      std::bind(&IFCFastIntAIChannel::start, this),
      std::bind(&IFCFastIntAIChannel::stop, this),
      std::bind(&IFCFastIntAIChannel::recover, this),
      std::bind(&IFCFastIntAIChannel::allowChange, this, std::placeholders::_1,
                std::placeholders::_2, std::placeholders::_3));

  m_node.addChild(m_stateMachine);

  // m_dataPV.setMaxElements(CHANNEL_SAMPLES_MAX);
  m_dataPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_dataPV);

  m_historyPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_historyPV);

  /* Add Set-point PV for Linear Conversion Factor */
  m_node.addChild(nds::PVDelegateOut<double>(
      "LinearConversionFactor",
      std::bind(&IFCFastIntAIChannel::setLinearConversionFactor, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getLinearConversionFactor, this,
                std::placeholders::_1, std::placeholders::_2)));
  /* Add Readback PV for Linear Conversion Factor */
  m_linConvFactorPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_linConvFactorPV);

  /* Add Set-point PV for Linear Conversion Offset */
  m_node.addChild(nds::PVDelegateOut<double>(
      "LinearConversionOffset",
      std::bind(&IFCFastIntAIChannel::setLinearConversionOffset, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getLinearConversionOffset, this,
                std::placeholders::_1, std::placeholders::_2)));
  /* Add Readback PV for Linear Conversion Offset */
  m_linConvOffsetPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_linConvOffsetPV);

  /* Add Set-point PV for Decimation Factor */
  m_node.addChild(nds::PVDelegateOut<std::int32_t>(
      "DecimationFactor",
      std::bind(&IFCFastIntAIChannel::setDecimationFactor, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getDecimationFactor, this,
                std::placeholders::_1, std::placeholders::_2)));
  /* Add Readback PV for Decimation Factor */
  m_decimationFactorPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_decimationFactorPV);

  /* Add Set-point PV for Gain */
  m_node.addChild(nds::PVDelegateOut<double>(
      "Gain", std::bind(&IFCFastIntAIChannel::setGain, this,
                        std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getGain, this, std::placeholders::_1,
                std::placeholders::_2)));
  /* Add Readback PV for Gain */
  m_gainPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_gainPV);

  /* Add Set-point PV for Test Pattern */
  nds::enumerationStrings_t testPatternStrings;
  testPatternStrings.push_back("NORMAL");
  testPatternStrings.push_back("ZERO");
  testPatternStrings.push_back("ONE");
  testPatternStrings.push_back("TOGGLE");
  testPatternStrings.push_back("RAMP INC");
  testPatternStrings.push_back("RAMP DEC");
  testPatternStrings.push_back("8PSIN");
  nds::PVDelegateOut<std::int32_t> node(nds::PVDelegateOut<std::int32_t>(
      "Pattern", std::bind(&IFCFastIntAIChannel::setPattern, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPattern, this, std::placeholders::_1,
                std::placeholders::_2)));
  node.setEnumeration(testPatternStrings);
  m_node.addChild(node);

  /* Add Readback PV for Test Pattern */
  m_patternPV.setScanType(nds::scanType_t::interrupt);
  m_patternPV.setEnumeration(testPatternStrings);
  m_node.addChild(m_patternPV);

  /* Add QOUT PV */
  m_pp_outPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_outPV);

  // m_node.addChild(m_pp_activePV);

  /* Add PV for PP emulated */
  node = nds::PVDelegateOut<std::int32_t>(
      "PP-EMUL", std::bind(&IFCFastIntAIChannel::setPPEmulated, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPPEmulated, this,
                std::placeholders::_1, std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_pp_emulatedPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_emulatedPV);

  /* Add PV for PP mode */
  node = nds::PVDelegateOut<std::int32_t>(
      "PP-MODE", std::bind(&IFCFastIntAIChannel::setPPMode, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPPMode, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_pp_modePV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_modePV);

  /* Add PV for PP val1 */
  nds::PVDelegateOut<double> node_d = nds::PVDelegateOut<double>(
      "PP-VAL1", std::bind(&IFCFastIntAIChannel::setPPVal1, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPPVal1, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node_d);
  /* Readback PV */
  m_pp_val1PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_val1PV);

  /* Add PV for PP val2*/
  node_d = nds::PVDelegateOut<double>(
      "PP-VAL2", std::bind(&IFCFastIntAIChannel::setPPVal2, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPPVal2, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node_d);
  /* Readback PV */
  m_pp_val2PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_val2PV);

  /* Add PV for PP val3*/
  node_d = nds::PVDelegateOut<double>(
      "PP-VAL3", std::bind(&IFCFastIntAIChannel::setPPVal3, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPPVal3, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node_d);
  /* Readback PV */
  m_pp_val3PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_val3PV);

  /* Add PV for PP val2*/
  node_d = nds::PVDelegateOut<double>(
      "PP-VAL4", std::bind(&IFCFastIntAIChannel::setPPVal4, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPPVal4, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node_d);
  /* Readback PV */
  m_pp_val4PV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_val4PV);

  /* Add PV for PP CVAL*/
  node_d = nds::PVDelegateOut<double>(
      "PP-CVAL", std::bind(&IFCFastIntAIChannel::setPPCVal, this,
                           std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getPPCVal, this, std::placeholders::_1,
                std::placeholders::_2));
  m_node.addChild(node_d);
  /* Readback PV */
  m_pp_cvalPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_pp_cvalPV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "TRANS-IDLE2PRE",
      std::bind(&IFCFastIntAIChannel::setInterlockStateTransitionIdlePre, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionIdlePre, this,
                std::placeholders::_1, std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_fsmTransIdlePrePV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmTransIdlePrePV);

  /* Add PV for Interlock Transisition qualifier*/
  node = nds::PVDelegateOut<std::int32_t>(
      "TRANS-PRE2RUN",
      std::bind(&IFCFastIntAIChannel::setInterlockStateTransitionPreRun, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionPreRun, this,
                std::placeholders::_1, std::placeholders::_2));
  m_node.addChild(node);
  /* Readback PV */
  m_fsmTransPreRunPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_fsmTransPreRunPV);

  /* Set some default values from hardware */
  uint32_t resolution;
  double vref_max;
  ifcdaqdrv_get_resolution(&m_deviceUser, &resolution);
  ifcdaqdrv_get_vref_max(&m_deviceUser, &vref_max);

  // std::cout << vref_max << " " << resolution << std::endl;

  /* Calculate initial conversion factor to VREF MAX / INT_MAX or SHRT_MAX. */
  m_linConvFactor = m_linConvFactorNext =
      vref_max / ((1 << (resolution - 1)) - 1);

  // Clear the PP configuration settings
  // TODO(nc): should this be controlled from PVs?
  // struct ifcfastint_analog_option option;
  // memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  // ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, 0xff, &option);
}

/* Convert from [0=-1, 32k=0, 64k=1] to [-32k=-1, 0=0, 32k-1=1] */
inline int32_t raw_to_signed(int16_t raw) { return (int16_t)(raw - 32768); }

/* Convert from [-32k=-1, 0=0, 32k-1=1] to [0=-1, 32k=0, 64k=1] */
inline int16_t signed_to_raw(int32_t signd) { return (int16_t)(signd + 32768); }

void IFCFastIntAIChannel::setLinearConversionFactor(const timespec &timespec,
                                                    const double &value) {
  m_linConvFactorNext = value;
  m_linConvFactorChanged = true;
  commitParameters();
}

void IFCFastIntAIChannel::getLinearConversionFactor(timespec *timespec,
                                                    double *value) {
  *value = m_linConvFactor;
}

void IFCFastIntAIChannel::setLinearConversionOffset(const timespec &timespec,
                                                    const double &value) {
  m_linConvOffsetNext = value;
  m_linConvOffsetChanged = true;
  commitParameters();
}

void IFCFastIntAIChannel::getLinearConversionOffset(timespec *timespec,
                                                    double *value) {
  *value = m_linConvOffset;
}

void IFCFastIntAIChannel::setDecimationFactor(const timespec &timespec,
                                              const int32_t &value) {
  if (value < 1) {
    // IFCFastIntNDS_MSGWRN("Decimation Factor cannot be less than 1.");
    return;
  }
  m_decimationFactorNext = value;
  m_decimationFactorChanged = true;
  commitParameters();
}

void IFCFastIntAIChannel::getDecimationFactor(timespec *timespec,
                                              int32_t *value) {
  *value = m_decimationFactor;
}

void IFCFastIntAIChannel::setGain(const timespec &timespec,
                                  const double &value) {
  m_gainNext = value;
  m_gainChanged = true;
  commitParameters();
}

void IFCFastIntAIChannel::getGain(timespec *timespec, double *value) {
  *value = m_gain;
}

void IFCFastIntAIChannel::setPattern(const timespec &timespec,
                                     const int32_t &value) {
  m_patternNext = value;
  m_patternChanged = true;
  commitParameters();
}

void IFCFastIntAIChannel::getPattern(timespec *timespec, int32_t *value) {
  clock_gettime(CLOCK_REALTIME, timespec);
  *value = m_pattern;
}

// void IFCFastIntAIChannel::getPPActive(timespec *timespec, int32_t *value){
//}

void IFCFastIntAIChannel::setPPEmulated(const timespec &timespec,
                                        const int32_t &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.emulation_en = value;
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_EMULATION_EN_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_pp_emulatedPV.read(&now, &rb_val);
  m_pp_emulatedPV.push(now, rb_val);
}

void IFCFastIntAIChannel::getPPEmulated(timespec *timespec, int32_t *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);

  clock_gettime(CLOCK_REALTIME, timespec);
  *value = option.emulation_en;
}

void IFCFastIntAIChannel::setPPMode(const timespec &timespec,
                                    const int32_t &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  switch (value) {
  case 0:
    option.mode = ifcfastint_amode_zero;
    break;
  case 1:
    option.mode = ifcfastint_amode_one;
    break;
  case 2:
    option.mode = ifcfastint_amode_gte1;
    break;
  case 3:
    option.mode = ifcfastint_amode_lte2;
    break;
  case 4:
    option.mode = ifcfastint_amode_gte1_lte2;
    break;
  case 5:
    option.mode = ifcfastint_amode_pulse_mon;
    break;
  case 6:
    option.mode = ifcfastint_amode_pd_avg;
    break;
  case 7:
    option.mode = ifcfastint_amode_pd_sum;
    break;
  case 8:
    option.mode = ifcfastint_amode_dev_mon;
    break;
  }
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_MODE_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_pp_modePV.read(&now, &rb_val);
  m_pp_modePV.push(now, rb_val);
}

void IFCFastIntAIChannel::getPPMode(timespec *timespec, int32_t *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  clock_gettime(CLOCK_REALTIME, timespec);
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  switch (option.mode) {
  case ifcfastint_amode_zero:
    *value = 0;
    break;
  case ifcfastint_amode_one:
    *value = 1;
    break;
  case ifcfastint_amode_gte1:
    *value = 2;
    break;
  case ifcfastint_amode_lte2:
    *value = 3;
    break;
  case ifcfastint_amode_gte1_lte2:
    *value = 4;
    break;
  case ifcfastint_amode_pulse_mon:
    *value = 5;
    break;
  case ifcfastint_amode_pd_avg:
    *value = 6;
    break;
  case ifcfastint_amode_pd_sum:
    *value = 7;
    break;
  case ifcfastint_amode_dev_mon:
    *value = 8;
    break;
  }
}

void IFCFastIntAIChannel::setPPVal1(const timespec &timespec,
                                    const double &value) {
  ifcdaqdrv_status status;
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.val1 = (int16_t)((value - m_linConvOffset) / m_linConvFactor);
  option.val1 = signed_to_raw(option.val1);
  status = ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                         IFCFASTINT_ANALOG_VAL1_W, &option);
  if (status) {
    ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
  }

  // Trigger an update of the readback value from hardware
  double rb_val;
  struct timespec now = {0, 0};
  m_pp_val1PV.read(&now, &rb_val);
  m_pp_val1PV.push(now, rb_val);
}

void IFCFastIntAIChannel::getPPVal1(timespec *timespec, double *value) {
  int16_t val1;
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  val1 = raw_to_signed(option.val1);
  clock_gettime(CLOCK_REALTIME, timespec);
  *value = val1 *m_linConvFactor + m_linConvOffset;
}

void IFCFastIntAIChannel::setPPVal2(const timespec &timespec,
                                    const double &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.val2 = (int16_t)((value - m_linConvOffset) / m_linConvFactor);
  option.val2 = signed_to_raw(option.val2);
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_VAL2_W, &option);

  // Trigger an update of the readback value from hardware
  double rb_val;
  struct timespec now = {0, 0};
  m_pp_val2PV.read(&now, &rb_val);
  m_pp_val2PV.push(now, rb_val);
}

void IFCFastIntAIChannel::getPPVal2(timespec *timespec, double *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  int16_t val2;
  clock_gettime(CLOCK_REALTIME, timespec);
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  val2 = raw_to_signed(option.val2);
  *value = val2 *m_linConvFactor + m_linConvOffset;
}

void IFCFastIntAIChannel::setPPVal3(const timespec &timespec,
                                    const double &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.val3 = (int16_t)((value - m_linConvOffset) / m_linConvFactor);
  option.val3 = signed_to_raw(option.val3);
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_VAL3_W, &option);

  // Trigger an update of the readback value from hardware
  double rb_val;
  struct timespec now = {0, 0};
  m_pp_val3PV.read(&now, &rb_val);
  m_pp_val3PV.push(now, rb_val);
}

void IFCFastIntAIChannel::getPPVal3(timespec *timespec, double *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  int16_t val3;
  clock_gettime(CLOCK_REALTIME, timespec);
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  val3 = raw_to_signed(option.val3);
  *value = val3 *m_linConvFactor + m_linConvOffset;
}

void IFCFastIntAIChannel::setPPVal4(const timespec &timespec,
                                    const double &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.val4 = (int16_t)((value - m_linConvOffset) / m_linConvFactor);
  option.val4 = signed_to_raw(option.val4);
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_VAL4_W, &option);

  // Trigger an update of the readback value from hardware
  double rb_val;
  struct timespec now = {0, 0};
  m_pp_val4PV.read(&now, &rb_val);
  m_pp_val4PV.push(now, rb_val);
}

void IFCFastIntAIChannel::getPPVal4(timespec *timespec, double *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  int16_t val4;
  clock_gettime(CLOCK_REALTIME, timespec);
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  val4 = raw_to_signed(option.val4);
  *value = val4 *m_linConvFactor + m_linConvOffset;
}

void IFCFastIntAIChannel::setPPCVal(const timespec &timespec,
                                    const double &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.cval = (int16_t)((value - m_linConvOffset) / m_linConvFactor);
  option.cval = signed_to_raw(option.cval);
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_CVAL_W, &option);

  // Trigger an update of the readback value from hardware
  double rb_val;
  struct timespec now = {0, 0};
  m_pp_cvalPV.read(&now, &rb_val);
  m_pp_cvalPV.push(now, rb_val);
}

void IFCFastIntAIChannel::getPPCVal(timespec *timespec, double *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  int16_t cval;
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  cval = raw_to_signed(option.cval);
  *value = cval *m_linConvFactor + m_linConvOffset;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAIChannel::setInterlockStateTransitionIdlePre(
    const timespec &timespec, const int32_t &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.idle2pre = value & 1;
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_IDLE2PRE_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_fsmTransIdlePrePV.read(&now, &rb_val);
  m_fsmTransIdlePrePV.push(now, rb_val);
}

void IFCFastIntAIChannel::getInterlockStateTransitionIdlePre(timespec *timespec,
                                                             int32_t *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  *value = option.idle2pre;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAIChannel::setInterlockStateTransitionPreRun(
    const timespec &timespec, const int32_t &value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  option.pre2run = value;
  ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum,
                                IFCFASTINT_ANALOG_PRE2RUN_W, &option);

  // Trigger an update of the readback value from hardware
  int32_t rb_val;
  struct timespec now = {0, 0};
  m_fsmTransPreRunPV.read(&now, &rb_val);
  m_fsmTransPreRunPV.push(now, rb_val);
}

void IFCFastIntAIChannel::getInterlockStateTransitionPreRun(timespec *timespec,
                                                            int32_t *value) {
  struct ifcfastint_analog_option option;
  memset(&option, 0, sizeof(struct ifcfastint_analog_option));
  ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, &option);
  *value = option.pre2run;
  clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAIChannel::commitParameters(bool calledFromAcquisitionThread) {
  struct timespec now = {0, 0};
  ifcdaqdrv_status status;

  clock_gettime(CLOCK_REALTIME, &now);

  /* If the call comes from the acquisition thread it is certain that the device
   * is not armed. Otherwise, only allow changes if device is in on or going to
   * on. */

  /*
  if(!calledFromAcquisitionThread && (
          m_stateMachine.getLocalState() != nds::state_t::on &&
          m_stateMachine.getLocalState() != nds::state_t::stopping  &&
          m_stateMachine.getLocalState() != nds::state_t::initializing )) {
      return;
  }
  */

  if (m_linConvFactorChanged) {
    m_linConvFactorChanged = false;
    m_linConvFactor = m_linConvFactorNext;
    double tmp;
    m_linConvFactorPV.read(&now, &tmp);
    m_linConvFactorPV.push(now, tmp);
  }

  if (m_linConvOffsetChanged) {
    m_linConvOffsetChanged = false;
    m_linConvOffset = m_linConvOffsetNext;
    double tmp;
    m_linConvOffsetPV.read(&now, &tmp);
    m_linConvOffsetPV.push(now, tmp);
  }

  if (m_decimationFactorChanged) {
    m_decimationFactorChanged = false;
    m_decimationFactor = m_decimationFactorNext;
    int32_t tmp;
    m_decimationFactorPV.read(&now, &tmp);
    m_decimationFactorPV.push(now, tmp);
  }

  if (m_gainChanged) {
    m_gainChanged = false;
    status = ifcdaqdrv_set_gain(&m_deviceUser, m_channelNum, m_gainNext);
    if (!status) {
      double change = m_gain;
      ifcdaqdrv_get_gain(&m_deviceUser, m_channelNum, &m_gain);
      change = change / m_gain;
      m_linConvFactor = m_linConvFactorNext = m_linConvFactorNext * change;

      m_gainPV.push(now, m_gain);
      m_linConvFactorPV.push(now, m_linConvFactor);
    }
  }

  if (m_patternChanged) {
    m_patternChanged = false;
    status = ifcdaqdrv_set_pattern(&m_deviceUser, m_channelNum,
                                   (ifcdaqdrv_pattern)m_patternNext);
    if (!status) {
      m_pattern = m_patternNext;
      int32_t tmp;
      m_patternPV.read(&now, &tmp);
      m_patternPV.push(now, tmp);
    }
  }
}

void IFCFastIntAIChannel::switchOn() {
  m_decimationOffsetNext = 0;
  m_decimationOffsetChanged = true;
  m_decimationFactorNext = 1;
  m_decimationFactorChanged = true;
  commitParameters();
}
void IFCFastIntAIChannel::switchOff() {}
void IFCFastIntAIChannel::start() { m_firstReadout = true; }
void IFCFastIntAIChannel::stop() { commitParameters(); }

/** @brief Don't support recover */
void IFCFastIntAIChannel::recover() {
  throw nds::StateMachineRollBack("Cannot recover");
}

/** @brief Allow all transitions */
bool IFCFastIntAIChannel::allowChange(const nds::state_t, const nds::state_t,
                                      const nds::state_t) {
  return true;
}

void IFCFastIntAIChannel::reserve(size_t nframes) {
  m_history.resize(nframes);
  m_history_ptr = m_history.begin();
}

void IFCFastIntAIChannel::historyReset() {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  m_historyPV.push(now, m_history);
  m_history.clear();
}

void IFCFastIntAIChannel::historyAddValue(int32_t data) {
  // if(m_channelNum == 0) {
  // printf("adding %d %f\n", data, data * m_linConvFactor + m_linConvOffset);
  //}
  *m_history_ptr = data * m_linConvFactor + m_linConvOffset;
  m_history_ptr++;
  //m_history.push_back();
}
void IFCFastIntAIChannel::addValue(int32_t data) {
  if (m_stateMachine.getLocalState() != nds::state_t::running) {
    /* Print the warning message the first time this function is inappropriately
     * called */
    if (m_firstReadout) {
      m_firstReadout = false;
      ndsDebugStream(m_node) << "Channel " << m_channelNum << " not Running"
                             << std::endl;
    }
    return;
  }

  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);

  m_dataPV.push(now, data * m_linConvFactor + m_linConvOffset);
}

void IFCFastIntAIChannel::setPPOutput(bool qout) {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);

  m_pp_outPV.push(now, (int32_t)qout);
}

void IFCFastIntAIChannel::getPPQOut(timespec *timespec, int32_t *value) {}

/* The data PV should only be processed from within. Therefore this function
 * shouldn't do anything */
void IFCFastIntAIChannel::getHistory(timespec *timespec,
                                     std::vector<double> *value) {}
void IFCFastIntAIChannel::getData(timespec *timespec, double *value) {}

void IFCFastIntAIChannel::setState(nds::state_t newState) {
  m_stateMachine.setState(newState);
}
