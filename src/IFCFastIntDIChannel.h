#ifndef IFCFastIntDICHANNEL_H
#define IFCFastIntDICHANNEL_H

#include <nds3/nds.h>
#include <ifcfastintdrv.h>

class IFCFastIntDIChannel {
public:
  IFCFastIntDIChannel(const std::string &name, nds::Node &parentNode,
                      int32_t channelNum, struct ifcdaqdrv_usr &deviceUser);
  std::int32_t m_channelNum;

  void addValue(int32_t data);
  void getData(timespec *timespec, int32_t *value);
  void getHistory(timespec *timespec, std::vector<std::int32_t> *value);

  void historyAddValue(int32_t data);
  void historyReset();
  void reserve(size_t nframes);

  void setState(nds::state_t newState);

  void setPPOutput(bool qout);
  void getPPQOut(timespec *timespec, int32_t *value);

  void setPPEmulated(const timespec &timespec, const int32_t &value);
  void getPPEmulated(timespec *timespec, int32_t *value);

  void setPPMode(const timespec &timespec, const int32_t &value);
  void getPPMode(timespec *timespec, int32_t *value);

  void setPPCVal(const timespec &timespec, const int32_t &value);
  void getPPCVal(timespec *timespec, int32_t *value);

  void setPPVal1(const timespec &timespec, const int32_t &value);
  void getPPVal1(timespec *timespec, int32_t *value);

  void setPPVal2(const timespec &timespec, const int32_t &value);
  void getPPVal2(timespec *timespec, int32_t *value);

  void setInterlockStateTransitionIdlePre(const timespec &timespec,
                                          const int32_t &value);
  void getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value);

  void setInterlockStateTransitionPreRun(const timespec &timespec,
                                         const int32_t &value);
  void getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value);

private:
  struct ifcdaqdrv_usr &m_deviceUser;
  double m_data;
  std::vector<std::int32_t> m_history;
  std::vector<std::int32_t>::iterator m_history_ptr;
  nds::Node m_node;
  nds::StateMachine m_stateMachine;

  /* Variables for parameters */
  bool m_pp_out;

  /* PV for channel data */
  nds::PVDelegateIn<std::int32_t> m_dataPV;
  nds::PVDelegateIn<std::vector<std::int32_t>> m_historyPV;

  /* PVs for readback values */
  nds::PVDelegateIn<std::int32_t> m_pp_outPV;
  nds::PVDelegateIn<std::int32_t> m_pp_emulatedPV;
  nds::PVDelegateIn<std::int32_t> m_pp_modePV;
  nds::PVDelegateIn<std::int32_t> m_pp_val1PV;
  nds::PVDelegateIn<std::int32_t> m_pp_val2PV;
  nds::PVDelegateIn<std::int32_t> m_pp_cvalPV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransIdlePrePV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransPreRunPV;
};

#endif /* IFCFastIntDICHANNEL_H */
