#ifndef IFCFastIntAICHANNEL_H
#define IFCFastIntAICHANNEL_H

#include <nds3/nds.h>
#include <ifcfastintdrv.h>

class IFCFastIntAIChannel {
public:
  IFCFastIntAIChannel(const std::string &name, nds::Node &parentNode,
                      int32_t channelNum, struct ifcdaqdrv_usr &deviceUser,
                      double linConvFactor);

  std::int32_t m_channelNum;

  void addValue(int32_t data);
  void getData(timespec *timespec, double *value);
  void getHistory(timespec *timespec, std::vector<double> *value);

  void historyAddValue(int32_t data);
  void historyReset();
  void reserve(size_t nframes);

  void setState(nds::state_t newState);

  void setLinearConversionFactor(const timespec &timespec, const double &value);
  void getLinearConversionFactor(timespec *timespec, double *value);

  void setLinearConversionOffset(const timespec &timespec, const double &value);
  void getLinearConversionOffset(timespec *timespec, double *value);

  void setDecimationFactor(const timespec &timespec, const int32_t &value);
  void getDecimationFactor(timespec *timespec, int32_t *value);

  void setGain(const timespec &timespec, const double &value);
  void getGain(timespec *timespec, double *value);

  void setPattern(const timespec &timespec, const int32_t &value);
  void getPattern(timespec *timespec, int32_t *value);

  void setPPOutput(bool qout);
  void getPPQOut(timespec *timespec, int32_t *value);

  // void getPPActive(timespec *timespec, int32_t *value);

  void setPPEmulated(const timespec &timespec, const int32_t &value);
  void getPPEmulated(timespec *timespec, int32_t *value);

  void setPPMode(const timespec &timespec, const int32_t &value);
  void getPPMode(timespec *timespec, int32_t *value);

  void setPPVal1(const timespec &timespec, const double &value);
  void getPPVal1(timespec *timespec, double *value);

  void setPPVal2(const timespec &timespec, const double &value);
  void getPPVal2(timespec *timespec, double *value);

  void setPPVal3(const timespec &timespec, const double &value);
  void getPPVal3(timespec *timespec, double *value);

  void setPPVal4(const timespec &timespec, const double &value);
  void getPPVal4(timespec *timespec, double *value);

  void setPPCVal(const timespec &timespec, const double &value);
  void getPPCVal(timespec *timespec, double *value);

  void setInterlockStateTransitionIdlePre(const timespec &timespec,
                                          const int32_t &value);
  void getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value);

  void setInterlockStateTransitionPreRun(const timespec &timespec,
                                         const int32_t &value);
  void getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value);

  void commitParameters(bool calledFromAcquisitionThread = false);

private:
  struct ifcdaqdrv_usr &m_deviceUser;
  // int8_t *m_rawData;
  double m_data;
  std::vector<double> m_history;
  std::vector<double>::iterator m_history_ptr;
  nds::Node m_node;
  nds::StateMachine m_stateMachine;

  bool m_firstReadout; /* Flag to indicate that this is the first time data is
                          read out. */

  void switchOn();
  void switchOff();
  void start();
  void stop();
  void recover();
  bool allowChange(const nds::state_t, const nds::state_t, const nds::state_t);

  /* Variables for parameters */
  double m_linConvFactor;
  double m_linConvOffset;
  int32_t m_decimationFactor;
  int32_t m_decimationOffset;
  double m_gain;
  int32_t m_pattern;
  bool m_pp_out;
  /* Preprocssing sub-block */
  // bool    m_pp_active;
  bool m_pp_emulated;
  ifcfastint_amode m_pp_mode;
  double m_pp_val1;
  double m_pp_val2;
  double m_pp_val3;
  double m_pp_val4;
  double m_pp_cval;

  /* Flags for when parameters are changed */
  double m_linConvFactorNext;
  double m_linConvOffsetNext;
  int32_t m_decimationFactorNext;
  int32_t m_decimationOffsetNext;
  double m_gainNext;
  int32_t m_patternNext;

  bool m_linConvFactorChanged;
  bool m_linConvOffsetChanged;
  bool m_decimationFactorChanged;
  bool m_decimationOffsetChanged;
  bool m_gainChanged;
  bool m_patternChanged;

  /* PV for channel data */
  nds::PVDelegateIn<double> m_dataPV;
  nds::PVDelegateIn<std::vector<double>> m_historyPV;
  /* PVs for readback values */
  nds::PVDelegateIn<double> m_linConvFactorPV;
  nds::PVDelegateIn<double> m_linConvOffsetPV;
  nds::PVDelegateIn<std::int32_t> m_decimationFactorPV;
  nds::PVDelegateIn<double> m_gainPV;
  nds::PVDelegateIn<std::int32_t> m_patternPV;
  nds::PVDelegateIn<std::int32_t> m_pp_outPV;
  /* Preprocssing sub-block */
  // nds::PVDelegateIn<std::int32_t> m_pp_activePV;
  nds::PVDelegateIn<std::int32_t> m_pp_emulatedPV;
  nds::PVDelegateIn<std::int32_t> m_pp_modePV;
  nds::PVDelegateIn<double> m_pp_val1PV;
  nds::PVDelegateIn<double> m_pp_val2PV;
  nds::PVDelegateIn<double> m_pp_val3PV;
  nds::PVDelegateIn<double> m_pp_val4PV;
  nds::PVDelegateIn<double> m_pp_cvalPV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransIdlePrePV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransPreRunPV;
};

#endif /* IFCFastIntAICHANNEL_H */
