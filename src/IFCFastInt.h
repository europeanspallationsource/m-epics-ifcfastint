#ifndef IFCFastInt_H
#define IFCFastInt_H

#include <iostream>
#include <sstream>

/*
 * Helper macro to push text to user through the INFO pv
 *
 * Not allowed to be called from constructor (before node tree is initialized)
 *
 * @param text Has to be something that can be passed to std::string()
 */

#define IFCFastIntNDS_MSGWRN(text)                                             \
  do {                                                                         \
    struct timespec now;                                                       \
    clock_gettime(CLOCK_REALTIME, &now);                                       \
    m_infoPV.push(now, std::string(text));                                     \
    ndsWarningStream(m_node) << std::string(text) << "\n";                     \
  } while (0)

/** Status check helper that prints error message from userspace library,
 * sends a message to the MSGR record and returns ndsError. */
#define IFCFastIntNDS_STATUS_MSGERR(func, status)                              \
  do {                                                                         \
    std::ostringstream tmp;                                                    \
    tmp << (func) << " " << ifcdaqdrv_strerror(status) << (status);            \
    IFCFastIntNDS_MSGWRN(tmp.str());                                           \
  } while (0)

#define IFCFastIntNDS_STATUS_CHECK(func, status)                               \
  do {                                                                         \
    if ((status) != status_success) {                                          \
      IFCFastIntNDS_STATUS_MSGERR(func, status);                               \
      /*throw nds::NdsError("ifcdaqdrv returned error");*/                     \
    }                                                                          \
  } while (0)

#define AICHANNEL_NUMBER_MAX 20
#define DICHANNEL_NUMBER_MAX 32
// TODO: reallocate with every commitParameters instead. Issue: channels
// pointers has to be updated with every realloc.
#define AICHANNEL_SAMPLES_MAX (4 * 1024 * 1024)

#endif /* IFCFastInt_H */
