/**
 * @file IFCFastIntAIChannelGroup.h
 * @brief Header file defining the analog input channel group class.
 * @author nclaesson
 * @date 2016-03-29
 */

#ifndef IFCFastIntAICHANNELGROUP_H
#define IFCFastIntAICHANNELGROUP_H

#include <nds3/nds.h>

#include "IFCFastIntAIChannel.h"

/**
 * @brief IFCFastIntAIChannelGroup
 */
class IFCFastIntAIChannelGroup {
public:
  IFCFastIntAIChannelGroup(const std::string &name, nds::Node &parentNode,
                           ifcdaqdrv_usr &deviceUser, size_t numChannelOffset);

  nds::Port m_node;
  nds::StateMachine m_stateMachine;
  ifcdaqdrv_usr &m_deviceUser;
  uint32_t m_numChannels;
  std::vector<std::shared_ptr<IFCFastIntAIChannel>> m_AIChannels;

  std::vector<std::shared_ptr<IFCFastIntAIChannel>> getChannels();

  void getInfoMessage(timespec *timespec, std::string *value);
  void getTriggerButton(timespec *timespec, std::string *value);

  void setSampleRate(const timespec &timespec, const double &value);
  void getSampleRate(timespec *timespec, double *value);

  void getClockSource(timespec *timespec, int32_t *value);
  void setClockSource(const timespec &timespec, const int32_t &value);

  void getClockFrequency(timespec *timespec, double *value);
  void setClockFrequency(const timespec &timespec, const double &value);

  void getClockDivisor(timespec *timespec, int32_t *value);
  void setClockDivisor(const timespec &timespec, const int32_t &value);

  void setState(nds::state_t newState);

  void onSwitchOn();
  void onSwitchOff();
  void onStart();
  void onStop();
  void recover();
  bool allowChange(const nds::state_t currentLocal,
                   const nds::state_t currentGlobal,
                   const nds::state_t nextLocal);
  void commitParameters(bool calledFromAcquisitionThread = false);

private:
  double m_sampleRate;

  int32_t m_clockSource;
  double m_clockFrequency;
  int32_t m_clockDivisor;

  bool m_sampleRateChanged;

  bool m_clockSourceChanged;
  bool m_clockFrequencyChanged;
  bool m_clockDivisorChanged;

  nds::PVDelegateIn<std::string> m_infoPV;

  nds::PVDelegateIn<double> m_sampleRatePV;

  nds::PVDelegateIn<std::int32_t> m_clockSourcePV;
  nds::PVDelegateIn<double> m_clockFrequencyPV;
  nds::PVDelegateIn<std::int32_t> m_clockDivisorPV;
};

#endif /* IFCFastIntAICHANNELGROUP_H */
