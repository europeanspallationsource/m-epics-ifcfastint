#ifndef IFCFastIntDEVICE_H
#define IFCFastIntDEVICE_H

#include <nds3/nds.h>

#include <ifcdaqdrv.h>

#include "IFCFastIntInterlockFSM.h"

class IFCFastIntDevice {
public:
  IFCFastIntDevice(nds::Factory &factory, const std::string &deviceName,
                   const nds::namedParameters_t &parameters);
  ~IFCFastIntDevice();

private:
  std::shared_ptr<IFCFastIntInterlockFSM> m_IFSM;
  struct ifcdaqdrv_usr m_deviceUser;
  nds::Node m_node;
};

#endif /* IFCFastIntDEVICE_H */
