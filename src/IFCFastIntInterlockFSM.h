/**
 * @file IFCFastIntInterlockFSM.h
 * @brief Header file defining the analog input channel group class.
 * @author nclaesson
 * @date 2016-03-29
 */

#ifndef IFCFASTINTINTERLOCKFSM_H
#define IFCFASTINTINTERLOCKFSM_H

#include <nds3/nds.h>

#include "IFCFastIntAIChannelGroup.h"
#include "IFCFastIntDIChannelGroup.h"

/**
 * @brief IFCFastIntInterlockFSM
 */
class IFCFastIntInterlockFSM {
public:
  IFCFastIntInterlockFSM(const std::string &name, nds::Node &parentNode,
                         ifcdaqdrv_usr &deviceUser);

  nds::Port m_node;
  nds::StateMachine m_stateMachine;
  ifcdaqdrv_usr &m_deviceUser;
  std::vector<std::shared_ptr<IFCFastIntAIChannelGroup>> m_AIGroups;
  std::shared_ptr<IFCFastIntDIChannelGroup> m_DIGroup;
  std::shared_ptr<IFCFastIntDIChannelGroup> m_DOGroup;

  // void markAllParametersChanged();

  void sendCommand(const timespec &timespec, const std::string &value);
  void recvCommand(timespec *timespec, std::string *value);

  void getInfoMessage(timespec *timespec, std::string *value);

  void getNSamples(timespec *timespec, int32_t *value);
  void setNSamples(const timespec &timespec, const int32_t &value);

  void getFrequency(timespec *timespec, int32_t *value);
  void setFrequency(const timespec &timespec, const int32_t &value);

  void getPPLock(timespec *timespec, int32_t *value);
  // void setPPLock(const timespec &timespec, const int32_t &value);
  void updatePPLock();

  void getInterlockState(timespec *timespec, int32_t *value);

  void getFSMOut(timespec *timespec, int32_t *value);

  void onSwitchOn();
  void onSwitchOff();
  void onStart();
  void onStop();
  void recover();
  bool allowChange(const nds::state_t currentLocal,
                   const nds::state_t currentGlobal,
                   const nds::state_t nextLocal);
  // void commitParameters(bool calledFromAcquisitionThread = false);

private:
  int8_t *m_rawData;

  int32_t m_nFrames;
  int32_t m_fsmFrequency;
  uint32_t m_fsmOutChannelMask;
  bool m_ppconf_locked;

  nds::PVDelegateIn<std::string> m_infoPV;

  nds::PVDelegateIn<std::int32_t> m_nSamplesPV;
  nds::PVDelegateIn<std::int32_t> m_fsmFrequencyPV;

  // nds::PVDelegateIn<std::int32_t> m_abortPV;

  nds::PVDelegateIn<std::int32_t> m_intFSMPV;
  nds::PVDelegateIn<std::int32_t> m_pplockPV;

  nds::PVDelegateIn<std::int32_t> m_fsmOut0PV;
  nds::PVDelegateIn<std::int32_t> m_fsmOut1PV;
  nds::PVDelegateIn<std::int32_t> m_fsmOut2PV;
  nds::PVDelegateIn<std::int32_t> m_fsmOut3PV;

  nds::Thread m_acquisitionThread;

  void acquisitionLoop(int32_t nsamples);
  bool m_stop;
};

#endif /* IFCFASTINTINTERLOCKFSM_H */
