#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "IFCFastInt.h"
#include "IFCFastIntAIChannelGroup.h"
#include "IFCFastIntAIChannel.h"

IFCFastIntAIChannelGroup::IFCFastIntAIChannelGroup(const std::string &name,
                                                   nds::Node &parentNode,
                                                   ifcdaqdrv_usr &deviceUser,
                                                   size_t numChannelOffset)
    : m_node(nds::Port(name, nds::nodeType_t::generic)),
      m_deviceUser(deviceUser), m_sampleRateChanged(false),
      m_clockSourceChanged(true), m_clockFrequencyChanged(true),
      m_clockDivisorChanged(true),
      m_infoPV(nds::PVDelegateIn<std::string>(
          "InfoMessage",
          std::bind(&IFCFastIntAIChannelGroup::getInfoMessage, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_sampleRatePV(nds::PVDelegateIn<double>(
          "SampleRate-RB",
          std::bind(&IFCFastIntAIChannelGroup::getSampleRate, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_clockSourcePV(nds::PVDelegateIn<std::int32_t>(
          "ClockSource-RB",
          std::bind(&IFCFastIntAIChannelGroup::getClockSource, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_clockFrequencyPV(nds::PVDelegateIn<double>(
          "ClockFrequency-RB",
          std::bind(&IFCFastIntAIChannelGroup::getClockFrequency, this,
                    std::placeholders::_1, std::placeholders::_2))),
      m_clockDivisorPV(nds::PVDelegateIn<std::int32_t>(
          "ClockDivisor-RB",
          std::bind(&IFCFastIntAIChannelGroup::getClockDivisor, this,
                    std::placeholders::_1, std::placeholders::_2))) {
  ifcdaqdrv_status status;
  parentNode.addChild(m_node);

  status = ifcdaqdrv_get_nchannels(&m_deviceUser, &m_numChannels);

  for (size_t numChannel(numChannelOffset);
       numChannel < m_numChannels + numChannelOffset; ++numChannel) {
    std::ostringstream channelName;
    channelName << "CH" << numChannel;
    m_AIChannels.push_back(std::make_shared<IFCFastIntAIChannel>(
        channelName.str(), m_node, numChannel, m_deviceUser, 10.0 / SHRT_MAX));
  }

  // PVs for Sample rate
  m_node.addChild(nds::PVDelegateOut<double>(
      "SampleRate", std::bind(&IFCFastIntAIChannelGroup::setSampleRate, this,
                              std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannelGroup::getSampleRate, this,
                std::placeholders::_1, std::placeholders::_2)));

  m_sampleRatePV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_sampleRatePV);

  // PVs for Clock Source
  nds::enumerationStrings_t clockSourceStrings;
  clockSourceStrings.push_back("Internal");
  clockSourceStrings.push_back("External");
  nds::PVDelegateOut<std::int32_t> node = nds::PVDelegateOut<std::int32_t>(
      "ClockSource", std::bind(&IFCFastIntAIChannelGroup::setClockSource, this,
                               std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannelGroup::getClockSource, this,
                std::placeholders::_1, std::placeholders::_2));
  node.setEnumeration(clockSourceStrings);
  m_node.addChild(node);

  m_clockSourcePV.setScanType(nds::scanType_t::interrupt);
  m_clockSourcePV.setEnumeration(clockSourceStrings);
  m_node.addChild(m_clockSourcePV);

  // PVs for Clock Frequency
  m_node.addChild(nds::PVDelegateOut<double>(
      "ClockFrequency",
      std::bind(&IFCFastIntAIChannelGroup::setClockFrequency, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannelGroup::getClockFrequency, this,
                std::placeholders::_1, std::placeholders::_2)));

  m_clockFrequencyPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_clockFrequencyPV);

  // PVs for Clock Divisor
  m_node.addChild(nds::PVDelegateOut<std::int32_t>(
      "ClockDivisor",
      std::bind(&IFCFastIntAIChannelGroup::setClockDivisor, this,
                std::placeholders::_1, std::placeholders::_2),
      std::bind(&IFCFastIntAIChannelGroup::getClockDivisor, this,
                std::placeholders::_1, std::placeholders::_2)));

  m_clockDivisorPV.setScanType(nds::scanType_t::interrupt);
  m_node.addChild(m_clockDivisorPV);

  // PV for debug/info messages.
  m_infoPV.setScanType(nds::scanType_t::interrupt);
  m_infoPV.setMaxElements(512);
  m_node.addChild(m_infoPV);

  // Initialize some values from hardware
  ifcdaqdrv_get_clock_source(&deviceUser, (ifcdaqdrv_clock *)&m_clockSource);
  ifcdaqdrv_get_clock_frequency(&deviceUser, &m_clockFrequency);
  ifcdaqdrv_get_clock_divisor(&deviceUser, (uint32_t *)&m_clockDivisor);

  m_sampleRate = m_clockFrequency / m_clockDivisor;
}

void IFCFastIntAIChannelGroup::setState(nds::state_t newState) {
  for (auto channel : m_AIChannels) {
    channel->setState(newState);
  }
}

std::vector<std::shared_ptr<IFCFastIntAIChannel>>
IFCFastIntAIChannelGroup::getChannels() {
  return m_AIChannels;
}

/*
 * Round upwards to nearest power-of-2.
 */

static inline int ceil_pow2(unsigned number) {
  unsigned n = 1;
  unsigned i = number - 1;
  while (i) {
    n <<= 1;
    i >>= 1;
  }
  return n;
}

/*
 * Round upwards to nearest mibibytes
 */

static inline int ceil_mibi(unsigned number) {
  return (1 + (number - 1) / (1024 * 1024)) * 1024 * 1024;
}

void IFCFastIntAIChannelGroup::getTriggerButton(timespec *timespec,
                                                std::string *value) {}

void IFCFastIntAIChannelGroup::getInfoMessage(timespec *timespec,
                                              std::string *value) {
  std::ostringstream tmp;
  char manufacturer[100];
  ifcdaqdrv_get_manufacturer(&m_deviceUser, manufacturer, 100);
  char product_name[100];
  ifcdaqdrv_get_product_name(&m_deviceUser, product_name, 100);
  tmp << manufacturer << " " << product_name;
  *value = tmp.str();
}

#define MAX_SAMPLE_RATES 100

struct sample_rate {
  double frequency;
  uint32_t divisor;
  uint32_t decimation;
  uint32_t average;
  double sample_rate;
};

// First priority is sample_rate, second divisor
static int compare_sample_rates(const void *a, const void *b) {
  const struct sample_rate *da = (const struct sample_rate *)a;
  const struct sample_rate *db = (const struct sample_rate *)b;
  int32_t sample_diff =
      (da->sample_rate > db->sample_rate) - (da->sample_rate < db->sample_rate);
  if (!sample_diff) {
    return da->divisor - db->divisor;
  }
  return sample_diff;
}

void IFCFastIntAIChannelGroup::getSampleRate(timespec *timespec,
                                             double *value) {
  *value = m_sampleRate;
}
void IFCFastIntAIChannelGroup::setSampleRate(const timespec &timespec,
                                             const double &value) {
  if (value < 0) {
    IFCFastIntNDS_MSGWRN("Sample rate cannot be negative.");
    return;
  }

  m_sampleRate = value;
  m_sampleRateChanged = true;
  commitParameters();
}

void IFCFastIntAIChannelGroup::getClockSource(timespec *timespec,
                                              int32_t *value) {
  *value = m_clockSource;
}

void IFCFastIntAIChannelGroup::setClockSource(const timespec &timespec,
                                              const int32_t &value) {
  m_clockSource = value;
  m_clockSourceChanged = true;
  commitParameters();
}

void IFCFastIntAIChannelGroup::getClockFrequency(timespec *timespec,
                                                 double *value) {
  *value = m_clockFrequency;
}

void IFCFastIntAIChannelGroup::setClockFrequency(const timespec &timespec,
                                                 const double &value) {
  m_clockFrequency = value;
  m_clockFrequencyChanged = true;
  commitParameters();
}

void IFCFastIntAIChannelGroup::getClockDivisor(timespec *timespec,
                                               int32_t *value) {
  *value = m_clockDivisor;
}

void IFCFastIntAIChannelGroup::setClockDivisor(const timespec &timespec,
                                               const int32_t &value) {
  m_clockDivisor = value;
  m_clockDivisorChanged = true;
  commitParameters();
}

/* commit parameters to hardware.
 *
 * the scope application supports
 */

void IFCFastIntAIChannelGroup::commitParameters(
    bool calledFromAcquisitionThread) {
  struct timespec now = {0, 0};
  ifcdaqdrv_status status;

  clock_gettime(CLOCK_REALTIME, &now);

  /* If the call comes from the acquisition thread it is certain that the device
   * is not armed. Otherwise, only allow changes if device is in on or going to
   * on. */

  // if(!calledFromAcquisitionThread && (
  //        m_stateMachine.getLocalState() != nds::state_t::on &&
  //        m_stateMachine.getLocalState() != nds::state_t::stopping  &&
  //        m_stateMachine.getLocalState() != nds::state_t::initializing )) {
  //    return;
  //}

  /*
   * The board has a circular buffer so that values before the trigger can be
   *read out. This is specified as "number of
   * pre-trigger samples". The amount has to be an even eighth of the total
   *number of samples and it cannot be all
   * samples.
   *
   * The user should be able to enter an arbitrary amount of samples and
   *pre-trigger samples. Using the following
   * "algorithm" the device support will then try to find a valid configuration
   *to get the requested values.
   *
   * 1. First we calculate an intial pre-trigger samples quotient (ptq) to
   *estimate this eighth.
   * 2. If ptq is 8 (8 eighths are requested as pre trigger samples) then the
   *amount of samples are doubled so that
   *    ptq is closer to 4 eighths.
   * 3. If we then cannot satisfy the amount of post trigger samples, the number
   *of samples has to be doubled again.
   *
   * Obvioysly pre-trigger samples are not allowed to be larger than the total
   *amount of samples. This is enforced
   * in the setters earlier.
   */

  //    uint32_t nchannels;
  //    uint32_t sample_size;
  //    ifcdaqdrv_get_nchannels(&m_deviceUser, &nchannels);
  //    ifcdaqdrv_get_resolution(&m_deviceUser, &sample_size);
  //    sample_size /= 8;

  //    if(m_nSamplesChanged || m_triggerDelayChanged) {
  //        m_nSamplesChanged = false;
  //        m_triggerDelayChanged = false;
  //
  //        uint32_t nSamples = 0;
  //        if(m_nSamples <= 16*1024) {
  //            nSamples = ceil_pow2(m_nSamples);
  //        } else {
  //            // 1MiB gets us 65k samples (divide by nchannels and
  //            sample_size)
  //            nSamples = ceil_mibi(m_nSamples * nchannels * sample_size) /
  //            nchannels / sample_size;
  //        }
  //        uint32_t nPretrig = 0;
  //        if(nSamples < 1024) {
  //            nSamples = 1024;
  //        }
  //        if(m_triggerDelay < 0) {
  //            nPretrig = -m_triggerDelay;
  //        }
  //
  //        /* Calculate initial ptq, ceil(8*(npretrig/nsamples)) */
  //        uint32_t ptq = (8 * nPretrig + nSamples - 1) / nSamples;
  //
  //        /* The only valid values for ptq is 0..7. Double the number of
  //        samples to fit the requested number of
  //         * pre-trigger samples. */
  //        if(ptq >= 8) {
  //            ndsDebugStream(m_node) << "Doubling nsamples" << std::endl;
  //            nSamples *= 2;
  //            if(nSamples > 16*1024) {
  //                nSamples = ceil_mibi(nSamples * nchannels * sample_size) /
  //                nchannels / sample_size;
  //            }
  //            ptq = (8 * nPretrig + nSamples - 1) / nSamples;
  //        }
  //
  //        /* Check that we get enough post-trigger samples. Double the number
  //        of samples to get the requested number of
  //         * post-trigger samples. */
  //        if((8-ptq) * nSamples / 8 < m_nSamples - nPretrig) {
  //            ndsDebugStream(m_node) << "Doubling nsamples" << std::endl;
  //            nSamples *= 2;
  //            if(nSamples > 16*1024) {
  //                nSamples = ceil_mibi(nSamples * nchannels * sample_size) /
  //                nchannels / sample_size;
  //            }
  //            ptq = (8 * nPretrig + nSamples - 1) / nSamples;
  //        }
  //
  //        nPretrig = (nSamples * ptq) / 8;
  //
  //        ndsDebugStream(m_node) << "Number of samples: " << nSamples << ",
  //        Number of pre-trigger samples: " << nPretrig << std::endl;
  //
  //        status = ifcdaqdrv_set_nsamples(&m_deviceUser, nSamples);
  //        IFCFastIntNDS_STATUS_CHECK("set_nsamples", status);
  //        if(status != status_success){
  //            std::ostringstream tmp;
  //            tmp << "To many samples requested, tried " << nSamples << "
  //            samples with " << nPretrig << " pre-trigger samples.";
  //            IFCFastIntNDS_MSGWRN(tmp.str());
  //            /* Set the largets possible amount of samples. */
  //            do {
  //                nSamples >>= 1;
  //                status = ifcdaqdrv_set_nsamples(&m_deviceUser, nSamples);
  //            } while(status && nSamples>0);
  //            ifcdaqdrv_get_nsamples(&m_deviceUser, (uint32_t *)&m_nSamples);
  //            nPretrig = m_triggerDelay = 0;
  //        }
  //
  //        m_nSamplesPV.push(now, m_nSamples);
  //
  //        status = ifcdaqdrv_set_npretrig(&m_deviceUser, nPretrig);
  //        IFCFastIntNDS_STATUS_CHECK("set_npretrig", status);
  //        if(status) {
  //            status = ifcdaqdrv_set_npretrig(&m_deviceUser, 0);
  //            m_triggerDelay = 0;
  //            m_triggerDelayPV.push(now, 0);
  //        } else {
  //            m_triggerDelayPV.push(now, m_triggerDelay);
  //        }
  //
  //    }

  //    if(m_triggerThresholdChanged || m_triggerSourceChanged ||
  //    m_triggerEdgeChanged) {
  //        m_triggerThresholdChanged = false;
  //        m_triggerSourceChanged = false;
  //        m_triggerEdgeChanged = false;
  //
  //        ifcdaqdrv_trigger_type triggerType = ifcdaqdrv_trigger_none;
  //        int32_t threshold = 0;
  //        uint32_t mask = 0;
  //        uint32_t polarity = 0;
  //
  //        switch(m_triggerSource) {
  //        case 0:
  //        case 1:
  //        case 2:
  //        case 3:
  //        case 4:
  //        case 5:
  //        case 6:
  //        case 7:
  //            triggerType = ifcdaqdrv_trigger_frontpanel;
  //            mask = 1 << m_triggerSource;
  //            polarity = ((m_triggerEdge + 1) % 2) << m_triggerSource;
  //            struct timespec tmp_time;
  //            double linConvFactor;
  //            try {
  //                m_AIChannels.at(m_triggerSource)->getLinearConversionFactor(&tmp_time,
  //                &linConvFactor);
  //                threshold = m_triggerThreshold/linConvFactor;
  //            } catch (const std::out_of_range& oor) {
  //                // Nothing to do if channel doesn't exist.
  //            }
  //            break;
  //        case 8:
  //            triggerType = ifcdaqdrv_trigger_frontpanel;
  //            mask = 1 << 30;
  //            polarity = ((m_triggerEdge + 1) % 2) << 30;
  //            break;
  //        case 9:
  //            triggerType = ifcdaqdrv_trigger_backplane;
  //            break;
  //        case 10:
  //            triggerType = ifcdaqdrv_trigger_soft;
  //            break;
  //        default:
  //            triggerType = ifcdaqdrv_trigger_none;
  //        }
  //        status = ifcdaqdrv_set_trigger(&m_deviceUser, triggerType,
  //        threshold, mask, polarity);
  //        IFCFastIntNDS_STATUS_CHECK("set_trigger", status);
  //        if(!status) {
  //            m_triggerThresholdPV.push(now, m_triggerThreshold);
  //            m_triggerSourcePV.push(now, m_triggerSource);
  //            m_triggerEdgePV.push(now, m_triggerEdge);
  //        }
  //    }
  //
  //    if(m_triggerRepeatChanged && m_stateMachine.getLocalState() !=
  //    nds::state_t::running) {
  //        m_triggerRepeatChanged = false;
  //        m_triggerRepeatPV.push(now, m_triggerRepeat);
  //    }

  if (m_clockSourceChanged) {
    m_clockSourceChanged = false;
    status = ifcdaqdrv_set_clock_source(&m_deviceUser,
                                        (ifcdaqdrv_clock)m_clockSource);
    IFCFastIntNDS_STATUS_CHECK("set_clock_source", status);
    if (!status) {
      m_clockSourcePV.push(now, m_clockSource);
    }
  }

  if (m_clockFrequencyChanged) {
    m_clockFrequencyChanged = false;
    status = ifcdaqdrv_set_clock_frequency(&m_deviceUser, m_clockFrequency);
    IFCFastIntNDS_STATUS_CHECK("set_clock_frequency", status);
    if (!status) {
      m_clockFrequencyPV.push(now, m_clockFrequency);
      m_sampleRate = m_clockFrequency / m_clockDivisor;
      m_sampleRatePV.push(now, m_sampleRate);
    }
  }

  if (m_clockDivisorChanged) {
    m_clockDivisorChanged = false;
    status = ifcdaqdrv_set_clock_divisor(&m_deviceUser, m_clockDivisor);
    IFCFastIntNDS_STATUS_CHECK("set_clock_divisor", status);
    if (!status) {
      m_clockDivisorPV.push(now, m_clockDivisor);
      m_sampleRate = m_clockFrequency / m_clockDivisor;
      m_sampleRatePV.push(now, m_sampleRate);
    }
  }

  //    if(m_decimationChanged) {
  //        m_decimationChanged = false;
  //        status = ifcdaqdrv_set_decimation(&m_deviceUser, m_decimation);
  //        IFCFastIntNDS_STATUS_CHECK("set_decimation", status);
  //        if(!status) {
  //            m_decimationPV.push(now, m_decimation);
  //            m_sampleRate = m_clockFrequency/m_clockDivisor;
  //            m_sampleRatePV.push(now, m_sampleRate);
  //        }
  //    }
  //
  //    if(m_averagingChanged) {
  //        m_averagingChanged = false;
  //        status = ifcdaqdrv_set_average(&m_deviceUser, m_averaging);
  //        IFCFastIntNDS_STATUS_CHECK("set_average", status);
  //        if(!status) {
  //            m_averagingPV.push(now, m_averaging);
  //            m_sampleRate =
  //            m_clockFrequency/m_clockDivisor/m_averaging/m_decimation;
  //            m_sampleRatePV.push(now, m_sampleRate);
  //        }
  //    }

  if (m_sampleRateChanged) {
    return;
    m_sampleRateChanged = false;
    struct sample_rate sample_rates[MAX_SAMPLE_RATES] = {};
    double frequencies[5];
    size_t nfrequencies;
    uint32_t decimations[8];
    size_t ndecimations;
    uint32_t averages[8];
    size_t naverages;
    uint32_t *downsamples;
    size_t ndownsamples;
    uint32_t divisor, div_min, div_max;
    uint32_t i, j, nsample_rates;
    int32_t k;
    double sample_rate;

    status =
        ifcdaqdrv_get_clock_divisor_range(&m_deviceUser, &div_min, &div_max);
    IFCFastIntNDS_STATUS_CHECK("get_clock_divisor_limits", status);
    status = ifcdaqdrv_get_clock_frequencies_valid(
        &m_deviceUser, frequencies, sizeof(frequencies) / sizeof(double),
        &nfrequencies);
    IFCFastIntNDS_STATUS_CHECK("get_clock_frequency_valid", status);
    status = ifcdaqdrv_get_decimations_valid(
        &m_deviceUser, decimations, sizeof(decimations) / sizeof(uint32_t),
        &ndecimations);
    IFCFastIntNDS_STATUS_CHECK("get_hw_decimations", status);
    status = ifcdaqdrv_get_averages_valid(&m_deviceUser, averages,
                                          sizeof(averages) / sizeof(uint32_t),
                                          &naverages);
    IFCFastIntNDS_STATUS_CHECK("get_hw_averages", status);

    /*
     * Try to find the combination of clock frequency, clock divisor, decimation
     *and average which
     * is closest (but higher) to the requested sample rate.
     *
     * The algorithm is as follows:
     * 1. For every available clock frequency
     *      For every available downsample (decimation and average)
     *         Start with the highest divisor and test all divisors until there
     *is a sample rate higher than requested.
     *         If such a sample rate is found, add it to the list of sample
     *rates.
     * 2. Sort list of sample rates. Lowest sample rate first. If equal
     *prioritize lowest clock divisor.
     * 3. Pick the first combination in the list.
     */

    nsample_rates = 0;
    for (i = 0; i < nfrequencies; ++i) {
      for (j = 0; j < 2; ++j) {
        if (j == 0) {
          downsamples = decimations;
          ndownsamples = ndecimations;
        } else {
          downsamples = averages;
          ndownsamples = naverages;
        }
        for (k = ndownsamples - 1; k >= 0; --k) {
          sample_rates[nsample_rates].frequency = frequencies[i];
          sample_rates[nsample_rates].divisor = div_min;
          sample_rates[nsample_rates].sample_rate =
              frequencies[i] / downsamples[k] / div_min;
          sample_rates[nsample_rates].decimation = 1;
          sample_rates[nsample_rates].average = 1;
          for (divisor = div_max; divisor >= div_min; --divisor) {
            sample_rate = frequencies[i] / downsamples[k] / divisor;
            ndsDebugStream(m_node) << "Try Frequency: " << frequencies[i]
                                   << ", Divisor: " << divisor
                                   << ", Downsample: " << downsamples[i]
                                   << " : " << sample_rate << std::endl;
            if (sample_rate >= m_sampleRate) {
              sample_rates[nsample_rates].frequency = frequencies[i];
              sample_rates[nsample_rates].divisor = divisor;
              sample_rates[nsample_rates].sample_rate = sample_rate;
              if (j == 0) {
                sample_rates[nsample_rates].decimation = downsamples[k];
              } else {
                sample_rates[nsample_rates].average = downsamples[k];
              }
              ndsDebugStream(m_node)
                  << "OK Frequency: " << sample_rates[nsample_rates].frequency
                  << ", Divisor: " << sample_rates[nsample_rates].divisor
                  << ", Decimation: " << sample_rates[nsample_rates].decimation
                  << ", Average: " << sample_rates[nsample_rates].average
                  << ". Sample Rate: "
                  << sample_rates[nsample_rates].sample_rate << std::endl;
              nsample_rates++;
              break;
            }
          }
        }
      }
    }

    // Sort lowest sample rates firsts.
    qsort(sample_rates, nsample_rates, sizeof(struct sample_rate),
          compare_sample_rates);

    ndsInfoStream(m_node) << "Will set Frequency: " << sample_rates[0].frequency
                          << ", Divider: " << sample_rates[0].divisor
                          << ", Decimation: " << sample_rates[0].decimation
                          << ", Average: " << sample_rates[0].average
                          << ". Sample Rate: " << sample_rates[0].sample_rate
                          << std::endl;

    status =
        ifcdaqdrv_set_clock_frequency(&m_deviceUser, sample_rates[0].frequency);
    IFCFastIntNDS_STATUS_CHECK("set_clock_frequency", status);
    status =
        ifcdaqdrv_set_clock_divisor(&m_deviceUser, sample_rates[0].divisor);
    IFCFastIntNDS_STATUS_CHECK("set_clock_divisor", status);
    if (sample_rates[0].decimation > 1) {
      status = ifcdaqdrv_set_average(&m_deviceUser, 1);
      IFCFastIntNDS_STATUS_CHECK("set_average", status);
      status =
          ifcdaqdrv_set_decimation(&m_deviceUser, sample_rates[0].decimation);
      IFCFastIntNDS_STATUS_CHECK("set_decimation", status);
    } else {
      status = ifcdaqdrv_set_decimation(&m_deviceUser, 1);
      IFCFastIntNDS_STATUS_CHECK("set_decimation", status);
      status = ifcdaqdrv_set_average(&m_deviceUser, sample_rates[0].average);
      IFCFastIntNDS_STATUS_CHECK("set_average", status);
    }
    m_sampleRatePV.push(now, sample_rates[0].sample_rate);
    m_clockFrequencyPV.push(now, sample_rates[0].frequency);
    m_clockDivisorPV.push(now, (int32_t)sample_rates[0].divisor);
    //        m_averagingPV.push(now, (int32_t)sample_rates[0].average);
    //        m_decimationPV.push(now, (int32_t)sample_rates[0].decimation);
  }
}

void IFCFastIntAIChannelGroup::onSwitchOn() {
  // Enable all channels
  for (auto const &channel : m_AIChannels) {
    channel->setState(nds::state_t::on);
  }
  commitParameters();
}

void IFCFastIntAIChannelGroup::onSwitchOff() {
  // Disable all channels
  for (auto const &channel : m_AIChannels) {
    channel->setState(nds::state_t::off);
  }
}

void IFCFastIntAIChannelGroup::onStart() {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);

  //    if(!m_nSamples) {
  //        m_infoPV.push(now, std::string("Samples is 0, won't start."));
  //        throw nds::StateMachineRollBack("Samples is 0, won't start.");
  //    }
  //
  //    if(!m_triggerRepeat) {
  //        m_infoPV.push(now, std::string("Trigger Repeat is 0, won't
  //        start."));
  //        throw nds::StateMachineRollBack("Trigger Repeat is 0, won't
  //        start.");
  //    }

  /* Start all Channels */
  for (auto const &channel : m_AIChannels) {
    channel->setState(nds::state_t::running);
  }
}

void IFCFastIntAIChannelGroup::onStop() {
  //    m_stop = true;
  ifcdaqdrv_disarm_device(&m_deviceUser);

  //    m_acquisitionThread.join();
  // Stop channels
  for (auto const &channel : m_AIChannels) {
    channel->setState(nds::state_t::on);
  }

  commitParameters();
}

void IFCFastIntAIChannelGroup::recover() {
  throw nds::StateMachineRollBack("Cannot recover");
}

bool IFCFastIntAIChannelGroup::allowChange(const nds::state_t currentLocal,
                                           const nds::state_t currentGlobal,
                                           const nds::state_t nextLocal) {
  return true;
}
